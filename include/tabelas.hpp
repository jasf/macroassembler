#ifndef TABELAS_HPP
#define TABELAS_HPP

#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<tuple>
#include<set>
#include<map>
#include<fstream>
#include<functional>
#include<utility>
#include<stdexcept>

#include<boost/algorithm/string.hpp>

#include "util.hpp"
#include "exception.hpp"

typedef struct SymbolTableData{
    unsigned int codeDefinitionLine, objectCodeLine;
    int isConst, spaceVecLen; // -1 se não é const, 0 se const = 0, 1 se é const (qualquer número)
    bool defined, fromSectionData;
    SymbolTableData(unsigned int line, unsigned int oLine, int svecLen, bool initialDef, int initialConst, bool isData) : codeDefinitionLine(line), objectCodeLine(oLine), spaceVecLen(svecLen), defined(initialDef), isConst(initialConst), fromSectionData(isData){}
} SymbolTableData;

typedef std::map<std::string,util::InstructionSpec> DirectiveMap;
typedef std::map<std::string,util::InstructionSpec> InstructionMap;

class SymbolTable{
    private:
        std::map<std::string, SymbolTableData> symbolsMap;

    public:
        SymbolTable();

        bool hasSymbol(std::string symbolName);

        bool isDefined(std::string name);

        SymbolTableData getSymbolData(std::string name);
        
        SymbolTableData getSymbolDataNoError(std::string name);

        void insertSymbol(std::string name, SymbolTableData data);

        void createNewSymbol(std::string name, bool defined=false, unsigned int codeLine=0, unsigned int objectLine=0, int spaceVecLen=1, int isConst=-1, bool isData=false    );

        void setSymbolAsDefined(std::string name);

        std::map<std::string, SymbolTableData> getSymbolsMap(){return symbolsMap;}



};
#endif
