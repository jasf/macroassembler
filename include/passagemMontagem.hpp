#ifndef PASSAGEM_MONTAGEM
#define PASSAGEM_MONTAGEM

#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<tuple>
#include<set>
#include<map>
#include<fstream>
#include<functional>
#include<utility>
#include<stdexcept>

#include<boost/algorithm/string.hpp>

#include "passagem.hpp"
#include "passagemMacro.hpp"
#include "util.hpp"
#include "tokenParser.hpp"
#include "tabelas.hpp"

class PassagemMontagem_Learn : public PassagemAbstrata{
    /*Essa passagem faz duas coisas:
     * - se uma definicao de simbolo é encontrada, então adicionamos na tabela de simbolos e:
     * - removemos a definicao do simbolo do codigo
     *
     *   Tambem resolveremos CONST, SPACE e Section, removendo essas duas diretivas do codigo
     *
     *   A proxima passagem somente precisara ler os enderecos utilizados como argumento para substitui-los
     *   por seus valores*/
    private:
        void construct(){
        }

    public:
        PassagemMontagem_Learn(std::ifstream *inputFile, std::ofstream *outputFile) : PassagemAbstrata(inputFile, outputFile){
            construct();
        }
        PassagemMontagem_Learn(std::ifstream *inputFile, std::ofstream *outputFile, PassagemMacro_Solve *pms ) : 
            PassagemAbstrata(inputFile, outputFile, pms->getSymbolTable(), pms->getParser(), pms->getInstructionMap(), pms->getDirectiveMap()){
            construct();
        }
        PassagemMontagem_Learn(std::ifstream *inputFile, std::ofstream *outputFile, SymbolTable *st, TokenParser *ts,
                InstructionMap* im, DirectiveMap *dm ) : 
            PassagemAbstrata(inputFile, outputFile, st, ts, im, dm){
            construct();
        }


        void dealWith_SymbolDefinition(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);
        void dealWith_Space(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);
        void dealWith_Const(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);
        void dealWith_Instruction(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);
        void dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);


        void printSymbolTable();
};

class PassagemMontagem_Solve : public PassagemMontagem_Learn{
    private:
        void construct(){}
    public:
        PassagemMontagem_Solve(std::ifstream *inputFile, std::ofstream *outputFile) : PassagemMontagem_Learn(inputFile, outputFile){
        }
        PassagemMontagem_Solve(std::ifstream *inputFile, std::ofstream *outputFile, PassagemMontagem_Learn *pml ) : 
            PassagemMontagem_Learn(inputFile, outputFile, pml->getSymbolTable(), pml->getParser(), pml->getInstructionMap(), pml->getDirectiveMap()){
            construct();
        }

        void dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);
        void doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

};

#endif
