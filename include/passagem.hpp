#ifndef PASSAGEM_ABSTRATA
#define PASSAGEM_ABSTRATA

#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<tuple>
#include<set>
#include<map>
#include<fstream>
#include<functional>
#include<utility>
#include<stdexcept>

#include<boost/algorithm/string.hpp>

#include "util.hpp"
#include "exception.hpp"
#include "tokenParser.hpp"
#include "tabelas.hpp"


using namespace std;

class PassagemAbstrata{
    private:
        //para forçar que ninguém mais tenha acesso a escrita, faço os seguintes membors privados
        unsigned long int inputLineCounter, inputPositionCounter;
        unsigned long int outputLineCounter, outputPositionCounter;

        std::ifstream *inputF;
        std::ofstream *outputF;

        //toda a construção básica é feita aqui.
        //Isso é feito para termos dois contrutores, um básico e outro mais completo
        //o mais completo chama o básico e depois acrescenta funcionalidades
        void construc(std::ifstream *inputFile, std::ofstream *outputFile);



    protected:
        /*lineBreaker: recebe string contendo linhas e retorna vetor de pares do tipo <string, token type>.
         A string corresponde ao token. O lineBreaker é definido no construtor de passagemAbstrata.
         Quando fiz isso estava querendo utilizar lambda functions em c++*/
        std::function<std::vector<std::tuple<std::string, util::TOKEN_TYPE> > (std::string) > lineBreaker;
        //tabelas utilizadas
        SymbolTable* symbolTable;
        DirectiveMap* directiveMap;
        InstructionMap* instructionMap;
        //token parser quebra uma linha em tokens e os classifica
        TokenParser *parser;
        //controla se mensagens de log serão mostradas ou não
        bool logActive = false;
        //controla quais msgs de log são mostradas
        int verbosity;
        //threshold para não mostrar erros repetidos em diferentes passagens do mesmo tipo(por exemplo, passagem de
        //montagem solve e learn verão os mesmos erros léxicos)
        int logTh;
        
        //estrutura utilizada para mapear linhas entre entrada e saida
        vector<int> outputLineVec;
        map<int, int> inputLineMap;

        bool textMode = false;
        bool dataMode = false;
        bool existText = false;
        bool existData = false;
        bool existStop = false;

    public:

        //construtor padrao
        PassagemAbstrata(std::ifstream *inputFile, std::ofstream *outputFile);

        //para faciliar escrita de modulos teste
        PassagemAbstrata(std::ifstream *inputFile, std::ofstream *outputFile, SymbolTable* st, TokenParser *ps, InstructionMap *im, 
                DirectiveMap *dm);

        //executa passagem
        /*Executa irá ler uma linha, calcular seus tokens e chamar doSomething.
         * Se houver uma definição de label eu uma linha, execute procura a próxima linha até achar
         * uma instrução.
         *
         * Execute também trata section*/
        void execute();



        //para ler ou escrever em arquivo, chamar as funções a seguir
        //metodo lanca std::runtime_error
        std::string readLineMacro(int lines2Skip);//lines2Skip para poder falar onde tem a linha de declaracao da macro sem endmacro
        std::string readLine();
        void writeToOutput(std::string s);
        void writeToOutput(int d);

        void writeToOutputSpace(std::string s);
        void writeToOutputSpace(int d);

        void writeEndOfLine();
        void writeSpace(){*outputF << " ";}

        virtual void  doSomething(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > tokenList) = 0;




        void toogleLog(){logActive = !logActive;}

        void report(std::string msg){
            if(verbosity > 5)
                std::cout << msg << std::endl;
        }
        // this method will always print a msg
        //
        void log(std::string msg, int control=0){
            if (control <= logTh)
                std::cout << msg << std::endl;
        }

        void incrementPosition(int d);
        void incrementLineCounter(int d);

        void initiateDefaultIndexMap(string fileName);
        void initiateIndexMap(string fileName, map<int, int> mapa);
        void initiateIndexMap(string fileName, vector<int> vec);

        void setTextModeOn();
        void setDataModeOn();
        void setLogControl(int d) { logTh = d;}

        //as classes podem mudar o comportamento padrão de dealWith_Section 
        virtual void dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        //-----------------------------------
        //getters
        //-----------------------------------
        unsigned long int getCurrentILineCounter(){return inputLineCounter;}
        unsigned long int getCurrentIPositionCounter(){return inputPositionCounter;}
        unsigned long int getCurrentOLineCounter(){return outputLineCounter;}
        unsigned long int getCurrentOPositionCounter(){return outputPositionCounter;}
        unsigned long int getInputMapedLineCounter(){return inputLineMap[inputLineCounter];}
        DirectiveMap* getDirectiveMap(){return directiveMap;}
        InstructionMap* getInstructionMap(){ return  instructionMap;}
        TokenParser* getParser(){ return parser;}
        SymbolTable* getSymbolTable(){return symbolTable;}
        vector<int> getOutputLineVector(){ return outputLineVec;}
        map<int, int> getInputLineMap(){ return inputLineMap;}

        //-----------------------------------
        //setters
        //-----------------------------------
        void setVerbosity(int v){verbosity = v;}
        //recebe uma funcao do tipo (string) -> vetor de tuplas (string, TOKEN_TYPE)
        void setLineBreaker(std::function<std::vector<std::tuple<std::string, util::TOKEN_TYPE> > (std::string) > breaker){//
            lineBreaker = breaker;
        } 
        void setInputLineMap(map<int, int> ilm) { inputLineMap = ilm;}
        void setParser(TokenParser *p) {this->parser = p;}
        void setMaps(InstructionMap *im, DirectiveMap *dm){
            directiveMap = dm;
            instructionMap = im;
        }
        void setSymbolTable(SymbolTable *ref){ this->symbolTable = ref;}
        vector<util::TOKEN_TYPE> getTokensInLine(vector<tuple<string, util::TOKEN_TYPE>> lineTokens);
        std::set<util::TOKEN_TYPE> getTokenSetInLine(std::vector<std::tuple<std::string, util::TOKEN_TYPE>> lineTokens);


        void copyLineToOutput(std::vector<std::tuple<std::string, util::TOKEN_TYPE>> v);
        string isInstructionSymbolValid(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens, string instrucName, SymbolTableData data);
        bool isInstructionLineValid(vector<util::TOKEN_TYPE> line, string instructName);
        bool isDirectiveLineValid(vector<util::TOKEN_TYPE> line, string instructName);

        //retorna se uma sequencia de tokens é do tipo: &A, &B, ... , &Z
        bool isMacroVarSeq(vector<util::TOKEN_TYPE> seq);

        string getInstructionHelpMsg(string instruction);
        string getDirectiveHelpMsg(string instruction);

        string getCurrentLineInstructionHelp(vector<util::TOKEN_TYPE> ltt);
        string getCurrentLineInstructionHelp(vector<tuple<string, util::TOKEN_TYPE>> ltt);
};




#endif
