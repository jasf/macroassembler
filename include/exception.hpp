#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include<stdexcept>
#include<string>
#include<vector>
#include<iostream>
#include<typeinfo>

using namespace std;

enum class ErrType{LEXICAL, SYNTATIC, SEMANTIC, MULTIPLE, EndOfFile, NON_DEFINED};

class UnexpectedEOF : public runtime_error{
    public:
        UnexpectedEOF(string s) : runtime_error(s) {}
};

class AssemblerError : public std::runtime_error{
    protected:
        ErrType err_type;
    public:
        unsigned long int line, position;
        AssemblerError(string msg, ErrType eType = ErrType::NON_DEFINED, unsigned long int line=-1, unsigned long int position=-1) :
            runtime_error(msg),err_type(eType),line(line), position(position) {}
        ErrType getType(){return err_type;}
};

class LexicalError : public AssemblerError{
    public:
        LexicalError(std::string msg, unsigned long int line=-1, unsigned long int position=-1 ) : AssemblerError(msg, ErrType::LEXICAL, line, position) {}
};

class SyntaticError : public AssemblerError{
    public:
        SyntaticError(std::string msg, unsigned long int line=-1, unsigned long int position=-1 ) : AssemblerError(msg, ErrType::SYNTATIC, line, position){}
};

class SemanticError : public AssemblerError{
    public:
        SemanticError(string msg, unsigned long int line=-1, unsigned long int position=-1 ) : AssemblerError(msg, ErrType::SEMANTIC, line, position){}
};

class MultipleErrorsInLine : public runtime_error{
    public:
        std::vector<AssemblerError> errors;
        MultipleErrorsInLine(std::vector<AssemblerError> es) : runtime_error("multiple errors in line"), errors(es){}
        MultipleErrorsInLine() : runtime_error("multiple errors in line"){}
        void addError(AssemblerError e){
            errors.push_back(e);
        }
        string myWhat(int lineNumber);


};
#endif
