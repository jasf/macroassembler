#ifndef PASSAGEM_ZERO
#define PASSAGEM_ZERO

#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<tuple>
#include<set>
#include<map>
#include<fstream>
#include<functional>
#include<utility>
#include<stdexcept>

#include<boost/algorithm/string.hpp>

#include "util.hpp"
#include "passagem.hpp"
#include "tokenParser.hpp"
#include "tabelas.hpp"
#include "exception.hpp"


class PassagemZero : public PassagemAbstrata{
    /*
     *Essa passagem retira do código macros 'EQU' e 'IF'
     * Assumiu-se que um SIMBOLO de macro avaliado por IF deve ter sido definido anteriormente por EQU
     * */
    private:
        std::map<std::string, int> flagMap;
        int lines2Skip;
        void construct();

    public:
        PassagemZero(std::ifstream *inputFile, std::ofstream *outputFile) : PassagemAbstrata(inputFile, outputFile){
            construct();
        }

        //para faciliar escrita de modulos teste
        PassagemZero(std::ifstream *iF, std::ofstream *oF, SymbolTable* st, TokenParser *ps, InstructionMap *im, 
                DirectiveMap *dm) : PassagemAbstrata(iF, oF, st, ps, im, dm){construct();}

        void dealWith_EQU(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void dealWith_IF(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

};


#endif

