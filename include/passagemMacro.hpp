#ifndef PASSAGEM_MACRO
#define PASSAGEM_MACRO

#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<tuple>
#include<set>
#include<map>
#include<fstream>
#include<functional>
#include<utility>
#include<stdexcept>

#include<boost/algorithm/string.hpp>

#include "passagem.hpp"
#include "passagemZero.hpp"
#include "util.hpp"
#include "tokenParser.hpp"
#include "tabelas.hpp"

typedef struct MNT_ENTRY{
    unsigned int numArg;
    std::map<std::string, int> argMap;
    MNT_ENTRY(unsigned int n, std::map<std::string, int> m) : numArg(n), argMap(m) {}
    MNT_ENTRY() {}
}MNT_ENTRY;

class PassagemMacro_Learn : public PassagemAbstrata{
    /*
     *Essa passagem retira do codigo:
     - definicoes de MACRO
     - linhas contendo 'MACRO' e 'ENDMACRO'
      
     A proxima passagem de macro tera apenas que resolver as chamadas de macro
     * */
    private:
        void construct(){
            readingMacro = false;
            lines2Skip = 0;
        }
    protected:
        //MNT é um mapa string -> MNT_ENTRY. Cada entrada contém o número de argumentos e seus nomes
        std::map<std::string, MNT_ENTRY> MNT;
        //MDT é um mapa string -> vetor de linhas. Cada linha é uma linha de código de definição da macro 
        std::map<std::string, std::vector<std::string>> MDT;

        bool readingMacro;
        int lines2Skip;

    public:
        PassagemMacro_Learn(std::ifstream *inputFile, std::ofstream *outputFile) : PassagemAbstrata(inputFile, outputFile){
            construct();
        }

        PassagemMacro_Learn(std::ifstream *inputFile, std::ofstream *outputFile, PassagemZero *pz) :
            PassagemAbstrata(inputFile, outputFile, pz->getSymbolTable(), pz->getParser(), pz->getInstructionMap(), pz->getDirectiveMap()){
            construct();
        }

        PassagemMacro_Learn(std::ifstream *inputFile, std::ofstream *outputFile, SymbolTable *st, TokenParser *tp, InstructionMap *im, DirectiveMap *dm) :
            PassagemAbstrata(inputFile, outputFile, st, tp, im, dm){
            construct();
        }

        void dealWith_MACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void dealWith_ENDMACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        std::map<std::string, MNT_ENTRY> getMNT(){return MNT;}
        void setMNT(std::map<std::string, MNT_ENTRY> m){MNT = m;}

        std::map<std::string, std::vector<std::string>> getMDT(){return MDT;}
        void setMDT(std::map<std::string, std::vector<std::string>> m){MDT = m;}

        void printTables();
        void dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);
};


class PassagemMacro_Solve : public PassagemMacro_Learn{
    /*
     *Essa passagem resolve as macros, retirando do codigo todas as chamadas a macro e substituindo-as
     *pela sua definicao
     * */
    private:
        void construct(){
        }
    public:
        //se esse construtor for utilizado, chamar posteriormente os metodos setMNT e setMDT
        PassagemMacro_Solve(std::ifstream *inputFile, std::ofstream *outputFile) : PassagemMacro_Learn(inputFile, outputFile){
            construct();
        }
        //para construir uma passagem macro solve aparotr de uma learn
        PassagemMacro_Solve(std::ifstream *inputFile, std::ofstream *outputFile, PassagemMacro_Learn *pm0) :
            PassagemMacro_Learn(inputFile, outputFile, pm0->getSymbolTable(), pm0->getParser(), pm0->getInstructionMap(), pm0->getDirectiveMap()){
            construct();
            setMNT(pm0->getMNT());
            setMDT(pm0->getMDT());
        }

        //a passagem montagem solve precisara chamar esse construtor
        //se esse construtor for utilizado, chamar posteriormente os metodos setMNT e setMDT
        PassagemMacro_Solve(std::ifstream *inputFile, std::ofstream *outputFile, SymbolTable *st, TokenParser *tp, InstructionMap *im, DirectiveMap *dm) :
            PassagemMacro_Learn(inputFile, outputFile, st, tp, im, dm){
            construct();
        }

        void solveMacro(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void dealWith_MACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

        void dealWith_ENDMACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens);

};

#endif
