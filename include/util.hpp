#ifndef UTIL_HPP
#define UTIL_HPP

#include<iostream>
#include<string>
#include<map>
#include<vector>

#include<boost/lexical_cast.hpp>

using namespace std;

namespace util{

    enum class TOKEN_TYPE;

    class InstructionSpec {
        public:
            int operandos, code, tamanho;
            //um vetor de tokens para cada possibilidade
            std::vector<std::vector<TOKEN_TYPE>> expectedOrder;
            InstructionSpec(int op, int cod, int tam) : operandos(op), code(cod), tamanho(tam){ }
            InstructionSpec(int op, int cod, int tam, vector<TOKEN_TYPE> p) : operandos(op), code(cod), tamanho(tam){
                addPossiblePath(p); 
            }
            InstructionSpec() : operandos(-1), code(-1), tamanho(-1){ }
            void addPossiblePath(vector<TOKEN_TYPE> p){ expectedOrder.push_back(p);}
    };


    /** Retorna um mapa contendo as instrucoes válidas. As chaves são nome da instrução
     * (em letra minúscula) e o valor é uma estrutura contendo campos operacao, code e tamanho.[todos
     * inteiros]*/
    std::map<std::string, util::InstructionSpec> getInstructionMap();

    /** Retorna um mapa contendo as diretivas válidas. Especificações semelhantes a getInstructionMap()
     */
    std::map<std::string, util::InstructionSpec> getDiretivasMap();


    enum class TOKEN_TYPE{INVALID,NUM_DEC, NUM_HEX, SYMBOL_DEFINITION_INDICATOR, MACRO, SYMBOL, DIRECTIVE, INSTRUCTION,
    COMMENT_MARK, ARG_SEPARATOR, MACRO_SYMBOL, ADD_SYMBOL, EMPTY, MACRO_VAR_SEQ};

    enum class OP_TYPE{PRE, MCR, MONT, INVALID};
    OP_TYPE getOpType(std::string s);

    std::string toString(TOKEN_TYPE e);

    std::pair<TOKEN_TYPE, int> classifyStringNumber(std::string s);

    void printMap(std::vector<int> map, std::string name="[out/in]");

    void waitKey();

    void removeTempFiles(std::string temp_file);

    template<typename Ts, typename T> bool setHasElement(Ts s, T el){
        return s.find(el) != s.end();
    }
    
    std::string getTokenName(std::tuple<std::string, TOKEN_TYPE> t);

    TOKEN_TYPE getTokenType(std::tuple<std::string, TOKEN_TYPE> t);

    bool hasSectionInLine(vector<tuple<string, TOKEN_TYPE>> line);

    void printLineToScreen(vector<tuple<string, TOKEN_TYPE>> line);

    string toString(vector<tuple<string, TOKEN_TYPE>>);

    map<string, string> parseInput(char** argv, int argc);

    map<int, int> createLineMap(vector<int> inputLines, vector<int> outputLines);
    // bool is_empty(std::ifstream& pFile);


}


#endif
