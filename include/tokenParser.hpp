#ifndef TOKEN_PARSER_HPP
#define TOKEN_PARSER_HPP

#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<tuple>
#include<set>
#include<map>
#include<fstream>
#include<functional>
#include<utility>
#include<stdexcept>

#include<boost/algorithm/string.hpp>

#include "util.hpp"
#include "exception.hpp"
#include "tabelas.hpp"

class TokenParser{
    /**
     * Lista de criterios atualmente implementada:
     * -token nao pode ter mais do que 50 caracteres
     * -primeiro caracter nao pode ser numerico
     * -caracteres em geral podem ser: numeros, letras, LETRAS e '_'
     * */
    private:
        //lista de criterios; campo definido no construtor dessa class
        std::vector<std::function<bool (const std::string token)>> criteriaList;

        //contem dados sobre os caracteres valdos como primeiro caracter e caracter em geral
        std::set<char> validNameCaracteres, validNameFirstCaracteres;

        //adiciona criterio a lista de criterios
        void addCriteria(std::function<bool (const std::string token)> crt)
                {criteriaList.push_back(crt);}

        DirectiveMap *directiveMap;
        InstructionMap *instructionMap;

    public:
        //construtor inicializa a lista de criterios
        TokenParser();

        //diz se o token segue as regras estabelecidas ou nao
        bool isTokenValid(const std::string token);

        util::TOKEN_TYPE calssifyToken(std::string token);

        void setMaps(InstructionMap *m, DirectiveMap *mm);
};
#endif //TOKEN_PARSER_HPP
