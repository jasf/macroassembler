#include<passagem.hpp>
#include<string>
#include<vector>

using namespace std;
int main(){
    TokenParser parser;

    cout << parser.isTokenValid("Isabela").toString() << endl;
    cout << parser.isTokenValid("Juarez").toString() << endl;
    cout << parser.isTokenValid("Wendy").toString() << endl;

    cout << parser.isTokenValid("0Wendy").toString() << endl;
    cout << parser.isTokenValid("_Wendy").toString() << endl;
    cout << parser.isTokenValid("_Wendy1234512341234123412341234123412341234123412341234123412341234124").toString() << endl;
    return 0;

}
