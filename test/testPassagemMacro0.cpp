//g++ --std=c++11 -I../include/ ../src/passagem.cpp ../src/util.cpp testPassagemMacro0.cpp  -o testMacro0
#include "passagem.hpp"
#include "passagemZero.hpp"
#include "passagemMacro.hpp"
#include "passagemMontagem.hpp"
#include "util.hpp"
#include <string>
#include <iostream>
#include <tuple>
#include <fstream>

using namespace std;



int main(){

    string baseName = "./test_code_pMacro";
    ifstream in(baseName + ".asm");
    ofstream out(baseName + ".pre");
    auto dTable = util::getDiretivasMap();
    auto iTable = util::getInstructionMap();
    SymbolTable sTable;
    TokenParser parser;

    PassagemZero p(&in, &out);
    p.setSymbolTable(&sTable);
    p.setParser(&parser);
    p.setMaps(&iTable, &dTable);

    try{
        p.execute();

        ifstream in(baseName + ".pre");
        ofstream out(baseName + ".mcr");
        PassagemMacro_Learn p(&in, &out);
        p.setSymbolTable(&sTable);
        p.setParser(&parser);
        p.setMaps(&iTable, &dTable);

        p.execute();
        in.close();
        out.close();

        auto MNT = p.getMNT();
        auto MDT = p.getMDT();

        for (auto entry : MNT)
            cout << entry.first << " numArgs: " << entry.second.numArg << endl;
        for (auto entry : MDT){
            cout << "macro name " << entry.first <<endl;
            for(auto entry2 : entry.second)
                cout << entry2 << endl;
            }

    }

    
    catch(InvalidDirectiveSyntax *E){
        cout << E->what() << endl;
    }

    return 0;
}

