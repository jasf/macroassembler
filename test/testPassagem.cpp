// compilar com g++ --std=c++11 -I../include/ ../bin/passagem.o ../bin/util.o testPassagem.cpp -o testPassagem
#include "passagem.hpp"
#include "util.hpp"
#include <string>
#include <iostream>
#include <tuple>
#include <fstream>

using namespace std;


class PassagemTeste : public PassagemAbstrata{
    public:
        PassagemTeste(std::ifstream *inputFile, std::ofstream *outputFile) : PassagemAbstrata(inputFile, outputFile){ }

        void doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
            for (auto token : lineTokens)
                cout << get<0>(token) << '\t' << util::toString(get<1>(token)) << endl;
        }
};

int main(){

    ifstream in("./test_code_p0.asm");
    ofstream out("./test_code.o");
    auto dTable = util::getDiretivasMap();
    auto iTable = util::getInstructionMap();
    SymbolTable sTable;
    TokenParser parser;

    PassagemTeste p(&in, &out);
    p.setSymbolTable(&sTable);
    p.setParser(&parser);
    p.setMaps(&iTable, &dTable);

    p.execute();

    return 0;
}
