#include<boost/lexical_cast.hpp>
#include <iostream>

bool is_hex_notation(std::string const& s)
{
  return s.compare(0, 2, "0x") == 0
      && s.size() > 2
      && s.find_first_not_of("0123456789abcdefABCDEF", 2) == std::string::npos;
}
using namespace std;
int main(){

    string in;
    cout << "entre um texto" << endl;
    cin >>  in;
    try{
        int in_d = boost::lexical_cast<int>(in);
        cout << "numero entrado " << in_d << endl;
    }
    catch(boost::bad_lexical_cast){
        if(is_hex_notation(in)){
            unsigned int x;   
            std::stringstream ss;
            ss << std::hex << in;
            ss >> x;
            cout << "numero entrado " << x << endl;
        }
        else
            cout << "not a number" << endl;


    }

}


