// g++ --std=c++11 -I../include/ ../src/passagem.cpp ../src/util.cpp testPassagemMacro1.cpp  -o testMacro1
#include "passagem.hpp"
#include "util.hpp"
#include "passagemZero.hpp"
#include "passagemMacro.hpp"
#include "passagemMontagem.hpp"
#include <string>
#include <iostream>
#include <tuple>
#include <fstream>
#include <stdexcept>

using namespace std;



int main(){

    string baseName = "./test_code_pMacro";
    ifstream in(baseName + ".asm");
    ofstream out(baseName + ".pre");
    auto dTable = util::getDiretivasMap();
    auto iTable = util::getInstructionMap();
    SymbolTable sTable;
    TokenParser parser;

    PassagemZero p(&in, &out);
    p.setSymbolTable(&sTable);
    p.setParser(&parser);
    p.setMaps(&iTable, &dTable);

    try{
        cout << "Executando passagem zero ... " << endl;
        p.execute();

        cout << "Executando primeira passagem de macro ... " << endl;
        ifstream in(baseName + ".pre");
        ofstream out(baseName + ".mcr");
        PassagemMacro_Learn p(&in, &out);
        p.setSymbolTable(&sTable);
        p.setParser(&parser);
        p.setMaps(&iTable, &dTable);

        p.execute();

        auto MNT = p.getMNT();
        auto MDT = p.getMDT();

        for (auto entry : MNT)
            cout << entry.first << " numArgs: " << entry.second.numArg << endl;
        for (auto entry : MDT){
            cout << "macro name " << entry.first <<endl;
            for(auto entry2 : entry.second)
                cout << entry2 << endl;
            }

        cout << "Executando segunda passagem ... " << endl;
        in.close();
        out.close();

        in.open(baseName + ".mcr");
        out.open(baseName + ".mcr2");

        PassagemMacro_Solve ps(&in, &out);
        ps.setSymbolTable(&sTable);
        ps.setParser(&parser);
        ps.setMaps(&iTable, &dTable);

        ps.setMNT(MNT);
        ps.setMDT(MDT);

        ps.execute();

        in.close();
        out.close();

        cout << "Executando primeira passagem de montagem ... " << endl;

        in.open(baseName + ".mcr2");
        out.open(baseName + ".o");

        PassagemMontagem_Learn pm0(&in, &out);
        pm0.setSymbolTable(&sTable);
        pm0.setParser(&parser);
        pm0.setMaps(&iTable, &dTable);

        pm0.toogleLog();
        pm0.execute();

        in.close();
        out.close();

        auto st = pm0.getSymbolTable()->getSymbolsMap();

        cout << "na tabela de simbolo" << endl;
        for(auto entry : st){
            cout << entry.first << endl;
            cout << entry.second.codeDefinitionLine << " " << entry.second.objectCodeLine << " " << entry.second.defined << endl;
        }
    }

    
    catch(std::runtime_error *E){
        cout << E->what() << endl;
    }

    return 0;
}


