// compilar com g++ --std=c++11 -I../include/ ../bin/passagem.o ../bin/util.o testPassagem.cpp -o testPassagem
#include "passagem.hpp"
#include "passagemZero.hpp"
#include "util.hpp"

#include <string>
#include <iostream>
#include <tuple>
#include <fstream>

using namespace std;

int main(){

    //count lines in file:
    ifstream in_aux("./triangulo.asm");
    int i = 0;
    string line;
    while(std::getline(in_aux, line))
        i++;

    in_aux.close();

    vector<int> initialMap;
    for ( int d = 0; d <= i; d++)
        initialMap.push_back(d);

    cout << "ok" << endl;

    ifstream in("./triangulo.asm");
    ofstream out("./triangulo.pre");

    auto dTable = util::getDiretivasMap();
    auto iTable = util::getInstructionMap();
    SymbolTable sTable;
    TokenParser parser;

    PassagemZero p(&in, &out);
    p.setSymbolTable(&sTable);
    p.setParser(&parser);
    p.setMaps(&iTable, &dTable);

    p.setInputLineMap(initialMap);


    try{
    p.toogleLog();
    p.execute();

    auto outputMap = p.getOutputLineMap();

    cout << "esse eh o mapeamento das linhas da saida para entrada" << endl;
    cout << "linha na saida : linha na entrada" << endl;

    for(int k = 0; k < outputMap.size(); k++)
        cout << k+1 << " " << outputMap[k] << endl;
    }
    catch(runtime_error E){
        cout << E.what() << endl;
    }

    return 0;
}

