#include <boost/algorithm/string.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <set>

using namespace std;

string preProcess(string s){
    string o = "";

    set<char> specialS;
    specialS.insert(':');
    specialS.insert(';');
    specialS.insert(',');

    for(auto ch:s){
        if (specialS.find(ch) != specialS.end())
            o += ' ';
        o += ch;
    }

    return o;
}
int main(){
    std::vector<std::string> strs;
    const string input = preProcess("ROT: INPUT N1; primeira linha do meu programa");
    boost::split(strs, input, boost::is_any_of("\t  "));
    for (auto e : strs)
        cout << e  << endl;

    return 0;
}
