// g++ --std=c++11 -I../include/ ../src/passagem.cpp ../src/util.cpp testPassagemMacro1.cpp  -o testMacro1
#include "passagem.hpp"
#include "util.hpp"
#include "passagemZero.hpp"
#include "passagemMacro.hpp"
#include "passagemMontagem.hpp"
#include <string>
#include <iostream>
#include <tuple>
#include <fstream>
#include <stdexcept>

using namespace std;



int main(){
    try{
        //inicializa variaveis dividias por todos os metodos
        auto dTable = util::getDiretivasMap();
        auto iTable = util::getInstructionMap();
        SymbolTable sTable;
        TokenParser parser;

        string baseName = "./triangulo";
        ifstream in;
        ofstream out;

        cout << "Executando passagem zero para resolver IF e EQU ... " << endl;
        in.open(baseName + ".asm"); out.open(baseName + ".pre");

        PassagemZero p0(&in, &out, &sTable, &parser, &iTable, &dTable);
        p0.initiateDefaultIndexMap(baseName + ".asm");
        p0.toogleLog();
        p0.execute();

        util::waitKey();

        cout << "Executando primeira passagem de macro ... " << endl;
        in.close(); out.close();
        in.open(baseName + ".pre"); out.open(baseName + ".mcr");

        //inicializa e roda
        PassagemMacro_Learn pm0(&in, &out, &p0);
        pm0.initiateDefaultIndexMap(baseName + ".pre");
        pm0.toogleLog();
        pm0.execute();

        //imprime resultados
        pm0.printTables();

        util::waitKey();

        cout << "Executando segunda passagem de macro ... " << endl;
        in.close(); out.close();
        in.open(baseName + ".mcr"); out.open(baseName + ".mcr2");

        //inicializa e roda
        PassagemMacro_Solve pm1(&in, &out, &pm0);
        pm1.initiateDefaultIndexMap(baseName + ".mcr");
        pm1.toogleLog();
        pm1.execute();

        util::waitKey();

        cout << "Executando primeira passagem de montagem ... " << endl;
        in.close(); out.close();
        in.open(baseName + ".mcr2"); out.open(baseName + ".o");

        //inicializa e executa
        PassagemMontagem_Learn pml(&in, &out, &pm1);
        pml.initiateDefaultIndexMap(baseName + ".mcr2");
        pml.toogleLog();
        pml.execute();

        //imprime resultados
        pml.printSymbolTable();

        util::waitKey();
        cout << "Executando segunda passagem de montagem ... " << endl;
        in.close(); out.close();
        in.open(baseName + ".o"); out.open(baseName + ".o2");

        PassagemMontagem_Solve pms(&in, &out, &pml);
        pms.initiateDefaultIndexMap(baseName + ".o");
        pms.toogleLog();
        pms.execute();

        in.close(); out.close();

        //get maps to print 
        auto outputMapZeroToIn = p0.getOutputLineMap();
        auto outputMapMacro0_2_Zero = pm0.getOutputLineMap();
        auto outputMapMacro1_2_Macro0 = pm1.getOutputLineMap();
        auto outputMapMonta0_2_Macro1 = pml.getOutputLineMap();
        auto outputMapMonta1_2_Monta0 = pms.getOutputLineMap();

        //print maps
        util::printMap(outputMapZeroToIn, "zero/input");
        util::printMap(outputMapMacro0_2_Zero, "macro0/zero");
        util::printMap(outputMapMacro1_2_Macro0, "macro1/macro0");
        util::printMap(outputMapMonta0_2_Macro1, "monta0/macro1");
        util::printMap(outputMapMonta1_2_Monta0, "monta1/monta0");

    }

    
    catch(std::runtime_error *E){
        cout << E->what() << endl;
    }

    return 0;
}


