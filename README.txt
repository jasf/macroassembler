Juarez Aires Sampaio Filho (11/0032829)
Jonathan Mendes de Almeida (12/0014581)

Compilação:
Para compilar todos os módulos do projeto, basta usar o comando:
    $ make
(na pasta src)

Uso:
=> Compilar assembly inventado
Basta usar o comando:
    $ ./montador -op entrada saida
em que -op pode ser: 
    -p para realizar apenas o pré-processamento e gerar o arquivo saida.pre
    -m para realizar apenas o pré-processamento e o tratamento de macros e gerar os arquivos saida.pre e saida.mcr
    -o para realizar a montagem completa, gerando saida.pre, saida.mcr e saida.o
	
	ps: o arquivo de entrada deve estar na mesma pasta do executável do montador e deve ser um arquivo .asm (não precisa escrever .asm na hora de rodar o programa)
	ex: nome do arquivo é entrada.asm
		para rodar você devera usar o comando: ./montador -o entrada nome_qualquer_de_saida

	ps2: a biblioteca boost está dentro da pasta “include” (caso dê algum problema, favor entrar em contato: jonathanalmd@gmail.com)

_____________________________________________________________________________________
obs: o montador precisa da biblioteca boost do c++
	o formato hexadecimal aceito pelo montador é começando com 0x ... 
		0xFFFF
	
	a pasta arquivosTeste possui alguns programas que funcionam (fatorial, triângulo, fibonacci…) que foram testados com o montador e estão funcionando perfeitamente.
	dentro da pasta arquivosTeste há uma chamada erros, com vários arquivos com os diversos erros possíveis. O montador consegue identificar todos os erros desses arquivos de teste.
	os testes foram realizados utilizando o simulador do Emanuel.