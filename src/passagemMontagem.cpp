#include "passagemMontagem.hpp"

void PassagemMontagem_Learn::dealWith_SymbolDefinition(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    report("[definition]");
    //testa se primeiro tipo é simbolo e segundo é ':'
    auto firstTokenT  = std::get<1>(lineTokens[0]);
    auto firstTokenS  = std::get<0>(lineTokens[0]);
    auto secondTokenT =  std::get<1>(lineTokens[1]);
    auto secondTokenS =  std::get<0>(lineTokens[1]);
    int foundSymbolDef = 0;

    for(int i = 0; i < lineTokens.size(); i++){
        if(std::get<1>(lineTokens[i]) == util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR)
            foundSymbolDef++;
    }
    if(foundSymbolDef > 1){
        throw SyntaticError("Dois rótulos ou mais na mesma linha");
    }

    if(firstTokenT != util::TOKEN_TYPE::SYMBOL)
        throw SyntaticError("Primeiro argumento de definicao de simbolo deve ser simbolo! Recebido " + firstTokenS +
                " do tipo " + util::toString(firstTokenT));
    if(secondTokenT != util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR)
        throw SyntaticError("Segundo argumento de definicao de simbolo deve ser : ! Recebido: " + secondTokenS +
                " do tipo " + util::toString(secondTokenT));

    //testa se ha algo depois. Esperamos que sempre haja, já que a função execute sempre
    //contatena definição de símbolo com próxima linha(até achar algo não vazio)
    if( lineTokens.size() < 3)
        throw SyntaticError("Definicao de simbolo espera algo em seguida!");

    //testa se simbolo já foi definido
    if(symbolTable->hasSymbol(secondTokenS))
        throw SemanticError("Redefinição de Simbolo '" + secondTokenS + "'. Previamente definido na linha " + 
            std::to_string(symbolTable->getSymbolData(secondTokenS).codeDefinitionLine));
    

    //testa terceiro tipo; pode ser instrucao, const ou space
    util::TOKEN_TYPE thirdTokenT  = std::get<1>(lineTokens[2]);
    auto thirdTokenS  = std::get<0>(lineTokens[2]);

    switch(thirdTokenT){
        case util::TOKEN_TYPE::DIRECTIVE :
            {
            if( thirdTokenS.compare("const") == 0){
                if( textMode == true || dataMode == false)
                    throw SemanticError("Definicao de const fora do bloco de dados!");
                dealWith_Const(lineTokens);
            }
            else if( thirdTokenS.compare("space") == 0){
                if( textMode == true || dataMode == false)
                    throw SemanticError("Definicao de space fora do bloco de dados!");
                dealWith_Space(lineTokens);
            }
            else
                throw SyntaticError("Terceiro argumento e definicao de simbolo errado! Esperava const ou space! Recebido " + thirdTokenS +
                " do tipo " + util::toString(thirdTokenT));
            break;
            }
        case util::TOKEN_TYPE::INSTRUCTION : 
            {
            if( textMode == false || dataMode == true)
                throw SemanticError("Definicao de simbolo para texto de codigo fora do bloco de texto!");

            //pega codigo da diretiva
            util::InstructionSpec spec = (*instructionMap)[thirdTokenS];
            int opSize = spec.tamanho;
            int numArgs = spec.operandos;
            
            //testa numero de operandos
            //testamos aqui pq vou acrescentar o cursor de posição pelo tamanho guardado na tabela
            //de símbolos. Os tamanhos devem ser iguais
            

            //escreve instrucao
            writeToOutput(thirdTokenS);
            writeSpace();

            //escreve os operandos (em formato texto, pois ainda nao os conhecemos com certeza)
            for(auto i = 3; i < lineTokens.size(); i++){
                writeToOutput(std::get<0>(lineTokens[i]));
                writeSpace();
            }

            //imprime fim de linha
            writeEndOfLine();

            //adiciona na tabela de simbolos
            auto symbolName = std::get<0>(lineTokens[0]);
            symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), -1, true, -1, false));
            //incrementa contador de programa
            incrementPosition(opSize);
            if (lineTokens.size() < 3 + numArgs)
                throw SyntaticError("# de argumentos inválido! Instrução " + thirdTokenS + " esperava " + std::to_string(numArgs) +
                        " operandos "); 
            break;
            }
        default:
            {
            //escreve instrucao
            writeToOutput(thirdTokenS);
            writeSpace();

            //escreve os operandos (em formato texto, pois ainda nao os conhecemos com certeza)
            int argPresent = 0;
            for(auto i = 3; i < lineTokens.size(); i++){
                writeToOutput(std::get<0>(lineTokens[i]));
                writeSpace();
                if(getTokenType(lineTokens[i]) == util::TOKEN_TYPE::SYMBOL)
                    argPresent++;
            }

            //imprime fim de linha
            writeEndOfLine();
            if(dataMode == false && textMode == false)
                throw SyntaticError("Definicao de simbolo invalida! Uso errado de diretiva de pre-processamento. Recebido " +
                    thirdTokenS + " do tipo " + util::toString(thirdTokenT) + ". Por conta desse erro, label não foi definido e causará problemas futuros pois será interpretado como uma instrução");
            throw SyntaticError("Definicao de simbolo invalida! Terceiro argumento aceito: instrução, space ou const. Recebido " +
                    thirdTokenS + " do tipo " + util::toString(thirdTokenT) + ". Por conta desse erro, label não foi definido");
            //adiciona na tabela de simbolos
            auto symbolName = std::get<0>(lineTokens[0]);
            if(dataMode == true && textMode == false){
                symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), -1, true, -1, true));
            }
            else{
                symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), -1, true, -1, false));     
            }
            //incrementa contador de programa
            incrementPosition(argPresent);
             
            break;
        }
    
    }
}
void PassagemMontagem_Learn::dealWith_Space(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    report("[space]");
    //se chegamos aqui, já testamos por forma: 'simbolo : space'; e o simbolo eh unico
    auto symbolName = std::get<0>(lineTokens[0]);

    //testa se temos quarto token, se tiver, esse será o tamanho do vtor
    if(lineTokens.size() == 4 ){
        //testa se tipo eh numerico
        auto quartoTokenT = std::get<1>(lineTokens[3]);
        auto quartoTokenS = std::get<0>(lineTokens[3]);
        //quarto argumento deve ser decimal(nesse ponto do código não há mais hexadecimal)
        if(quartoTokenT == util::TOKEN_TYPE::NUM_DEC ){
            //a saida dessa funcao.second corresponde ao valor inteiro contido no string de entrada
            //(a saber: o .first corresponde ao tipo, se é dec ou hex
            auto numeral = util::classifyStringNumber(quartoTokenS).second; 
            for(int i = 0; i < numeral; i++){
                writeToOutput("0");
                writeEndOfLine();
            }
            symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), numeral, true, -1, true));
            incrementPosition(numeral);
        }
        else
            throw SyntaticError("Uso inválido de diretiva space! Quarto argumento deveria ser numeral decimal");
    
    }
    //nesse caso temos a forma: 'nome : space'
    else{
        writeToOutput("0");
        writeEndOfLine();
        symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), 1, true, -1, true));
        incrementPosition(1);
    }
}
void PassagemMontagem_Learn::dealWith_Const(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    report("[const]");
    //se chegamos aqui, já testamos por forma: 'simbolo : space'; e o simbolo eh unico
    auto symbolName = std::get<0>(lineTokens[0]);

    //testa se temos quarto token
    if(lineTokens.size() == 4 ){
        //testa se tipo eh numerico
        auto quartoTokenT = std::get<1>(lineTokens[3]);
        auto quartoTokenS = std::get<0>(lineTokens[3]);
        if(quartoTokenT == util::TOKEN_TYPE::NUM_DEC || quartoTokenT == util::TOKEN_TYPE::NUM_HEX){
            auto numeral = util::classifyStringNumber(quartoTokenS).second; 
            writeToOutput(std::to_string(numeral));
            writeEndOfLine();
            if(numeral == 0)
                symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), -1, true, 0, true));
            else
                symbolTable->insertSymbol(symbolName, SymbolTableData(getCurrentILineCounter(), getCurrentIPositionCounter(), -1, true, 1, true));
            
            incrementPosition(1);
        }
        else
            throw SyntaticError("Uso inválido de diretiva const! Deveriamos ter quarto argumento numerico!");
    
    }
    else
        throw SyntaticError("Uso inválido de diretiva const! Deveriamos ter quarto argumento numerico!");
}

void PassagemMontagem_Learn::dealWith_Instruction(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    report("[instruction]");

    auto instruct = std::get<0>(lineTokens[0]);
    //será que a instruçao realmente existe?
    if(instructionMap->find(instruct) != instructionMap->end()){
        //pega codigo da diretiva
        util::InstructionSpec spec = (*instructionMap)[instruct];
        int opSize = spec.tamanho;
        int numArgs = spec.operandos;
        
        //testa numero de operandos
        //lembre que podemos ter load B + 2, nesse caso, teríamos 4 tokens, sendo que load espera ter apenas tamanho 2
        //no entanto, vai funcior: esse B+2 deve ocupar apenas uma posição no código objeto final
        //acho que o ideal seria aqui contar o número de operandos que são símbolos
        if (lineTokens.size() < 1 + numArgs)
            throw SyntaticError("# de argumentos inválido! Instrução " + instruct + " esperava " + std::to_string(numArgs) + " operandos "); 
        //escreve instrucao
        writeToOutput(instruct);
        writeSpace();

        //escreve os operandos (em formato texto, pois ainda nao os conhecemos com certeza)
        for(auto i = 1; i < lineTokens.size(); i++){
            writeToOutput(std::get<0>(lineTokens[i]));
            writeSpace();
        }

        //imprime fim de linha
        writeEndOfLine();

        //incrementa contador de programa
        incrementPosition(opSize);
    }
    else{//caso a instrução não exista, andamos o contador posição o número de argumentos na linha
        writeToOutput(instruct);
        writeSpace();

        int symbolCount = 0;
        //escreve os operandos (em formato texto, pois ainda nao os conhecemos com certeza)
        for(auto i = 1; i < lineTokens.size(); i++){
            writeToOutput(std::get<0>(lineTokens[i]));
            writeSpace();
            if(std::get<1>(lineTokens[i]) == util::TOKEN_TYPE::SYMBOL)
                symbolCount+= 1;
        }

        //imprime fim de linha
        writeEndOfLine();

        //incrementa contador de programa: um para a instrução, um para cada argumento
        incrementPosition(symbolCount+1);
    
    }

    if( textMode == false || dataMode == true)
        throw SemanticError("Uso de instrucao fora do bloco de texto!");
}
void PassagemMontagem_Learn::doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    bool dealt=false;
    for(auto token : lineTokens){
        //pega informaca de tipo e nome do token
        std::string tokenS= std::get<0>(token);
        util::TOKEN_TYPE tokenT = std::get<1>(token);
        //testa se token define sessao, space, const ou se define simbolo
        if(tokenT == util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR){
            dealWith_SymbolDefinition(lineTokens);
            dealt = true;
            break;
        }
        else if(tokenT == util::TOKEN_TYPE::DIRECTIVE && tokenS.compare("section") == 0){
            dealWith_Section(lineTokens);
            dealt = true;
            break;
        }
    }

    //se a linha nao contiver nenhum dos casos anteriores, ira conter uma instrucao pura
    if(dealt == false)
        dealWith_Instruction(lineTokens);
}

void PassagemMontagem_Solve::doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    MultipleErrorsInLine errors;
    //nesse ponto teremos apenas instrucoes e simbolos e numeros
    //ainda mais, toda primeiro token deve ser uma instrucao!
    for(auto i = 0; i < lineTokens.size(); i++){
        auto token = lineTokens[i];

        //pega informaca de tipo e nome do token
        std::string tokenS= std::get<0>(token);
        util::TOKEN_TYPE tokenT = std::get<1>(token);
        report("<mnt solve>[] token:" + tokenS + ", TT:" + util::toString(tokenT)); 
        
        if(i == 0){
            //primeiro token deve ser intrucao ou numero, se for numero, somente pode haver um por linha
            if(tokenT != util::TOKEN_TYPE::INSTRUCTION){
                if(tokenT == util::TOKEN_TYPE::NUM_DEC){
                    if(lineTokens.size() > 1 && dataMode == false)
                        errors.addError(SyntaticError("Linha com dados deve conter apenas um dado!"));
                    }
                else if(dataMode == false){
                    errors.addError(SyntaticError("Primeiro token deve ser instrução! Token '" + tokenS + "' não é instrução válida"));
                    // errors.addError(SyntaticError("Primeiro token deve ser instrução! Token '" +\
                                // tokenS + "' do tipo " + util::toString(tokenT) +   " não é instrução válida"));
                }
            }
        }
        //ignora virgulas
        if(tokenT == util::TOKEN_TYPE::ARG_SEPARATOR){
            report("[,]");
            continue;
        }
        else if(tokenT == util::TOKEN_TYPE::INSTRUCTION){
            report("<mnt solve>[instruction]" + tokenS);
            //testa se comando existe
            if(instructionMap->find(tokenS) == instructionMap->end()){
                errors.addError(SyntaticError("Instrucao '" + tokenS +  "' invalida!"));
                continue;
            }
            //compara tokens na linha com possíceis especificações
            util::InstructionSpec spec = (*instructionMap)[tokenS];
            auto specExpectedSyntax = spec.expectedOrder;
            auto lineT_only= getTokensInLine(lineTokens);
            if(!isInstructionLineValid(lineT_only, tokenS)){
                errors.addError(SyntaticError("Sequência de tokens recebida não bate com nenhuma especificação de instrução " + tokenS + 
                        + "\n" + getInstructionHelpMsg(tokenS) + getCurrentLineInstructionHelp(lineT_only)));
                continue;
            }
            else if(tokenS != "stop"){
                SymbolTableData data = symbolTable->getSymbolData(std::get<0>(lineTokens[1]));
                string isValid = isInstructionSymbolValid(lineTokens, tokenS, data);
                if(isValid != "true"){
                    if(isValid == "div"){
                        errors.addError(SemanticError("Divisão por zero"));
                    }
                    else if(isValid == "store"){ // adcionar tratamento do caso COPY e INPUT
                        errors.addError(SemanticError("Modificação de constante ou store em label inválido (label da section text)"));
                    }
                    else if(isValid == "jumpsintatico"){
                        errors.addError(SyntaticError("Jump não aceita LABEL + NUM_DEC"));
                    }
                    else if(isValid == "jumpsemantico"){
                        errors.addError(SemanticError("Jump para label da section data (jump para rótulo inválido)"));
                    }
                    else if(isValid == "input"){
                        errors.addError(SemanticError("Input com label de constante ou com rótulo da section text"));
                    }
                    else if(isValid == "copy1"){
                        errors.addError(SemanticError("Copy com segundo argumento constante ou com rótulo da section text ou usando símbolo não definido"));
                    }
                    else if(isValid == "copy2"){
                        errors.addError(SemanticError("Copy com segundo argumento constante ou com rótulo da section text"));
                        errors.addError(SemanticError("Uso de instrução "+  tokenS + " com rótulo da section text"));
                    }
                    else if(isValid == "instruc"){
                        errors.addError(SemanticError("Uso de instrução "+  tokenS + " com rótulo da section text"));
                    }
                }
            }
            if(tokenS == "stop")
                existStop = true;
            //pega codigo da instrucao
            int opCode = spec.code;

            //pega codigo da instrucao
            writeToOutput(opCode);
            writeSpace();

        }else if(tokenT == util::TOKEN_TYPE::SYMBOL && i > 0){//i > 0 para nao mostar erro semantico de simbolo nao definido para caso de instrucao errada
            report("[symb]");
            //testa se simbolo foi definido
            if(!symbolTable->hasSymbol(tokenS) ){
                errors.addError(SemanticError("Simbolo '" + tokenS +  "' nao definido!"));
                continue;
            
            }
            //pega endereço do simbolo
            auto addr = symbolTable->getSymbolData(tokenS).objectCodeLine;

            //testa se o próximo existe e é sinal de adição
            if( i + 1 < lineTokens.size() && get<1>(lineTokens[i+1]) == util::TOKEN_TYPE::ADD_SYMBOL){
                //testa se há próximo argumento e ele é numérico
                if( i + 2 < lineTokens.size() && get<1>(lineTokens[i+2]) == util::TOKEN_TYPE::NUM_DEC){
                    report("[symb+N identified]");
                    //nesse ponto devemos testar se o tamanho do vetor está sendo testado
                    unsigned int spaceVecLen = symbolTable->getSymbolData(tokenS).spaceVecLen;
                    unsigned int sumVec = stoi(get<0>(lineTokens[i+2]));
                    bool fromData = symbolTable->getSymbolData(tokenS).fromSectionData;
                    string instrucName =  std::get<0>(lineTokens[0]);
                    
                    if(spaceVecLen == -1){
                        errors.addError(SemanticError("Instrução usando forma 'INSTRUCTION SYMBOL + NUM_DEC' com SYMBOL que não aceita '+ NUM_DEC' ou então passando tamanho do vetor"));
                    }
                    else if(sumVec >= spaceVecLen && fromData){
                        errors.addError(SemanticError("Ultrapassando tamanho do vetor"));
                    }
                    addr += boost::lexical_cast<int>(get<0>(lineTokens[i+2]));
                    //pula próximos dois símbolos
                    i += 2;
                }
                else {
                    errors.addError(SyntaticError("Esperava decimal após sinal de +"));
                    continue;
                }
            }

            writeToOutput(addr);
            writeSpace();

        }else if(tokenT == util::TOKEN_TYPE::NUM_DEC){
            //simplesmente imprime numero no arquivo
            report("[num d]");
            writeToOutput(tokenS);
            writeSpace();
        }
    
    }

    if(errors.errors.size() > 0){
        throw MultipleErrorsInLine(errors.errors);
    }

}

void PassagemMontagem_Learn::printSymbolTable(){
        report( "na tabela de simbolo");
        for(auto entry : symbolTable->getSymbolsMap()){
            report( entry.first );
            report( std::to_string(entry.second.codeDefinitionLine) + " " +
                    std::to_string(entry.second.objectCodeLine) + " " + 
                    std::to_string(entry.second.defined) + " " +
                    std::to_string(entry.second.spaceVecLen) + " " +
                    std::to_string(entry.second.isConst) + " " +
                    std::to_string(entry.second.fromSectionData));
        }
}

void PassagemMontagem_Learn::dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    PassagemAbstrata::dealWith_Section(lineTokens);
    copyLineToOutput(lineTokens);
}

void PassagemMontagem_Solve::dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    PassagemAbstrata::dealWith_Section(lineTokens);
}
