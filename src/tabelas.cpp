 #include "tabelas.hpp"

SymbolTable::SymbolTable(){
}
bool SymbolTable::hasSymbol(std::string symbolName){
    return symbolsMap.find(symbolName) != symbolsMap.end();
}

SymbolTableData SymbolTable::getSymbolData(std::string name){
    auto target = symbolsMap.find(name);
    if (target == symbolsMap.end())
        throw SemanticError("Símbolo '" + name + "' não encontrado!");
    return target->second;
}

SymbolTableData SymbolTable::getSymbolDataNoError(std::string name){
    auto target = symbolsMap.find(name);
   
    return target->second;
}


void SymbolTable::insertSymbol(std::string name, SymbolTableData data){
    if (hasSymbol(name))
        throw SemanticError("Símbolo '" + name + "' redefinido!");
    symbolsMap.insert(std::make_pair(name, data));
}
void SymbolTable::setSymbolAsDefined(std::string name){
    auto target = symbolsMap.find(name);
    if (target == symbolsMap.end())
        throw SemanticError("Símbolo '" + name + "' não encontrado!");
    target->second.defined = true;
}

void SymbolTable::createNewSymbol(std::string name, bool defined, unsigned int codeLine, unsigned int objectLine, int spaceVecLen, int isConst, bool isData){
   this->insertSymbol(name, SymbolTableData(codeLine, objectLine, spaceVecLen, defined, isConst, isData)); 
}

bool SymbolTable::isDefined(std::string name){
    auto target = symbolsMap.find(name);
    if (target == symbolsMap.end())
        throw SemanticError("Símbolo '" + name + "' não encontrado!");
    return target->second.defined;
}
