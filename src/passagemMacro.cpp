#include "passagemMacro.hpp"

void PassagemMacro_Learn::dealWith_MACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    //get macro name
    std::string macroName = std::get<0>(lineTokens[0]);

    auto lineT_only = getTokensInLine(lineTokens);

    if(!isDirectiveLineValid(lineT_only, "macro"))
        throw SyntaticError("Diretiva macro com formato inválido!\n" + getDirectiveHelpMsg("macro") +  getCurrentLineInstructionHelp(lineT_only));


    //pega lista de argumentos
    std::map<std::string, int> argumentos;
    int count = 0;
    for (unsigned int i = 3; i < lineTokens.size();){
        //pega informacoes da variavel
        std::string arg = std::get<0>(lineTokens[i]);
        //testa por argumento de macro repetido
        if(argumentos.find(arg) != argumentos.end())
            throw SemanticError("Argumento de Macro '" + arg+ "' Repetido!"); 
        //pega de 2 em 2 para pular vírgula
        i+=2;
        argumentos.insert(std::make_pair(arg, count));
        count++;
    }

    int numOfArg = count;
    MNT_ENTRY mnt_entry(numOfArg, argumentos);
    MNT.insert(std::make_pair(macroName, mnt_entry));

    //read file until a 'endmacro' is found
    std::string line;
    std::vector<std::string> macroDefinition;
    //vai termianr quando endmacro foi achado ou em excessao de fim de arquivo
    while(true){
        line = readLineMacro(lines2Skip);

        lines2Skip++;
        auto newLineTokens = lineBreaker(line);
        //get info on the first entry
        auto firstEntry = newLineTokens[0];
        auto fES = std::get<0>(firstEntry);
        boost::to_lower(fES);
        auto fET = std::get<1>(firstEntry);
        //test if first entry is endmacro
        if (fET == util::TOKEN_TYPE::DIRECTIVE && fES.compare("endmacro")==0)
            break;
        else
            macroDefinition.push_back(line);
    }

    MDT.insert(std::make_pair(macroName, macroDefinition));

}

void PassagemMacro_Learn::dealWith_ENDMACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    throw SemanticError("Diretiva ENDMACRO sem nenhum MACRO acompanhante!");
}

void PassagemMacro_Learn::doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    bool dealt = false;
    for (auto token : lineTokens){
        std::string tokenS= std::get<0>(token);
        util::TOKEN_TYPE tokenT = std::get<1>(token);
        boost::algorithm::to_lower(tokenS);
        if(tokenT == util::TOKEN_TYPE::DIRECTIVE && tokenS.compare("macro") == 0){
            dealWith_MACRO(lineTokens);
            dealt = true;
            break;
        }
        if(tokenT == util::TOKEN_TYPE::DIRECTIVE && tokenS.compare("endmacro") == 0){
            dealWith_ENDMACRO(lineTokens);
            dealt = true;
            break;
        }
    }
    if(dealt == false){
        for(auto token : lineTokens){
            writeToOutput(std::get<0>(token));
            writeSpace();
        }
        writeEndOfLine();
    }
}

/*Essa funcação assume que o caso especial label: macro já foi tratado anteriormente.
 * Isto é, a linha a ser resolvida contém somente: 'macroname (vars)*/
void PassagemMacro_Solve::solveMacro(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    //pega nome da macro
    std::string macroName = getTokenName(lineTokens[0]);
    report("<mcr solve>[resolvendo macro] " + macroName);

    //pega lista de argumentos na chamada de macro
    std::map<std::string, int> argumentos;
    int count = 0;
    //começa de i=1 pq i=0 corresponde ao nome da macro
    for (unsigned int i = 1; i < lineTokens.size();){
        //pega informacoes da variavel
        std::string arg = getTokenName(lineTokens[i]);
        util::TOKEN_TYPE arg_e = getTokenType(lineTokens[i]);
        //test for type of argument, it must be a SYMBOL
        if (arg_e != util::TOKEN_TYPE::SYMBOL)
            throw SyntaticError("Variaveis de chamada MACRO devem ser simbolicas. Variavel recebida '" + arg + "' do tipo recebido: " + util::toString(arg_e)); 
        //testa por argumento de macro repetido
        if(argumentos.find(arg) != argumentos.end())
            throw SemanticError("Argumento de chamada de Macro '" + arg+ "' Repetido!"); 

        //testa se proximo token eh do tipo separador de variavel ','
        if(i+1 < lineTokens.size()){
            std::string argNext = getTokenName(lineTokens[i+1]);
            util::TOKEN_TYPE argN_e = getTokenType(lineTokens[i+1]);
            //test for type of argument, it must be a SYMBOL_SEPARATOR
            if (argN_e != util::TOKEN_TYPE::ARG_SEPARATOR)
                throw SyntaticError("Variaveis de chamda de MACRO devem ser separadas por ','.\
                        Token recebida '" + argNext + "' do tipo recebido: " + util::toString(argN_e)); 
        }
        i+=2;
        argumentos.insert(std::make_pair(arg, count++));
    }
   
    //testa se numero de argumentos bate com experado
    auto expectedCount = MNT.find(macroName)->second.numArg; 
    if( count != expectedCount)
        throw SemanticError("Chamada de macro com # de argumentos inesperado! Esperava-se " + std::to_string(expectedCount) + "; recebido " + std::to_string(count));

    //pega mapa que mapeia &MacroVar para index da macro var
    std::map<std::string, int> directDefMap = MNT[macroName].argMap;

    //constroi mapa reverso das variaveis na chamada de macro 
    std::map<int, std::string> reverseLocalMap;
    for(auto entry : argumentos)
        reverseLocalMap[entry.second] = entry.first;

    //le definicao da macro linha por linha
    for(auto line : MDT[macroName]){
        auto currentLikeTokens = lineBreaker(line);
        for(auto c_token : currentLikeTokens){
            //pega informacoes da variavel
            std::string c_tok_s = std::get<0>(c_token);
            util::TOKEN_TYPE c_tok_t = std::get<1>(c_token);

            //testa se eh simbolo de macro
            if(c_tok_t == util::TOKEN_TYPE::MACRO_SYMBOL){
                //se for, mapeia a dimbolo macro para o correspondente simbolo local
                auto indexOfCurrentMacroSym = directDefMap[c_tok_s];
                auto strToWrite = reverseLocalMap[indexOfCurrentMacroSym];
                
                writeToOutput(strToWrite);
                writeSpace();
            }
            else{
                writeToOutput(c_tok_s);
                writeSpace();
            }
        }
    writeEndOfLine();
    }
}



void PassagemMacro_Solve::doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    bool dealt=false;
    for(auto token : lineTokens){
        //pega informaca de tipo e nome do token
        std::string tokenS= std::get<0>(token);
        util::TOKEN_TYPE tokenT = std::get<1>(token);
    
        //testa se token eh simbolo e se ele esta na MNT
        if(tokenT == util::TOKEN_TYPE::SYMBOL && (MNT.find(tokenS) != MNT.end())){

            //resolve problema da linha que possui macro + label. ex: label: swap A B
            //Nesse ponto do codigo, as unicas ocorrencias de ':' sao para definicao de label
            auto tokenSet = getTokenSetInLine(lineTokens);

            if((lineTokens.size() > 2) && util::setHasElement(tokenSet, util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR)){
                //so para ter certeza: o primeiro token da linha deve ser um token e o segundo deve ser o ':'
                auto firstTokenT = util::getTokenType(lineTokens[0]);
                auto secondTokenT = util::getTokenType(lineTokens[1]);
                if( firstTokenT == util::TOKEN_TYPE::SYMBOL && secondTokenT == util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR){
                    report("<mcr solve>[log]: caso especial label : macro...");
                    //imprime a lable e os dois pontos
                    writeToOutputSpace(util::getTokenName(lineTokens[0]));
                    writeToOutputSpace(":");

                    //remove primeiros dois tokens da linha porque ja lidamos com eles
                    //a funcao de solveMacro pode entao considerar que a macro vai ser a primeira coisa em lineTokens
                    lineTokens.erase(lineTokens.begin(), lineTokens.begin()+2);
                }
            }

            solveMacro(lineTokens);
            dealt = true;
            break;
        }
    }

    if(dealt == false){
        for(auto token : lineTokens){
            writeToOutputSpace(std::get<0>(token));
        }
        writeEndOfLine();
    }
}


void PassagemMacro_Solve::dealWith_MACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    //read file until a 'endmacro' is found
    std::string line;
    while(true){
        line = readLine();
        lines2Skip++;
        auto newLineTokens = lineBreaker(line);
        //get info on the first entry
        auto firstEntry = newLineTokens[0];
        auto fES = std::get<0>(firstEntry);
        boost::to_lower(fES);
        auto fET = std::get<1>(firstEntry);
        //test if first entry is endmacro
        if (fET == util::TOKEN_TYPE::DIRECTIVE && fES.compare("endmacro")==0)
            break;

    }

}

void PassagemMacro_Solve::dealWith_ENDMACRO(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    throw SemanticError("Diretiva ENDMACRO sem nenhum MACRO acompanhante!");
}

void PassagemMacro_Learn::printTables(){
        for (auto entry : MNT)
            report(entry.first + " numArgs: " + to_string(entry.second.numArg));
        for (auto entry : MDT){
            report("macro name " + entry.first);
            for(auto entry2 : entry.second)
                report(entry2);
            }
}

void PassagemMacro_Learn::dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    PassagemAbstrata::dealWith_Section(lineTokens);
    copyLineToOutput(lineTokens);

}
