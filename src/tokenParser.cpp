#include"tokenParser.hpp"

TokenParser::TokenParser(){
    //token nao pode ter mais que 50 caracteres
    addCriteria( [](const std::string token){
            return token.size() <= 50;
            });

    //cria array com valor inteiro correspondente aos caracteres validos
    const int numerais[] = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57};
    const int maisculas[] = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76,
     77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90};
    const int minusculas[] = {97, 98, 99, 100, 101, 102, 103, 104, 105, 106,
 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};

    //numerais, maiusculas, minusculas e _ pertencem aos caracteres validos;
    //numerais nao pertence aos caracteres validos para o primeiro caracter
    for (auto n : numerais)
        validNameCaracteres.insert(n);

    for (auto n : maisculas){
        validNameCaracteres.insert(n);
        validNameFirstCaracteres.insert(n);
    }

    for (auto n : minusculas){
        validNameCaracteres.insert(n);
        validNameFirstCaracteres.insert(n);
    }

    //primeira letra de variaveis pode ser '_'
    validNameCaracteres.insert(int('_'));
    validNameFirstCaracteres.insert(int('_'));

    //primeira letra de variaveis de macro sempre eh '&'
    validNameFirstCaracteres.insert(int('&'));

    //primeira letra do token deve pertencer ao conjunto de primeiras letras validas
    addCriteria( [&](const std::string token){
            const char firstChar = token[0];
            if (this->validNameFirstCaracteres.find(firstChar) != this->validNameFirstCaracteres.end())
                return true;
            else
                throw std::string("primeiro caracter '") + firstChar + "' invalido!";

            });

    //todos os caracteres devem pertencer ao conjunto de caracteres válidos
    addCriteria( [&](const std::string token){
            bool result = true;
            char ch_d;
            std::string msgToSend = std::string("Token valido!");
            for(auto i = 1; i<token.size(); i++){
                auto ch = token[i];
                ch_d = ch; 
                if( this->validNameCaracteres.find(ch_d) == this->validNameCaracteres.end()){
                    result = false;
                    throw std::string("Caracter '") + ch_d + "' invalido!";
                    }
                }
                return result;
            });
}

bool TokenParser::isTokenValid(const std::string token){
        try{
            for(auto crt : criteriaList)
                crt(token);
            }catch(std::string E){
                //decidit o que fazer com string de msg depois
                //cout << E << endl;
                return false;
            }
        return true;
}


util::TOKEN_TYPE TokenParser::calssifyToken(std::string token){
    //testa se token é numérico
    if (util::classifyStringNumber(token).first != util::TOKEN_TYPE::INVALID) 
        return util::classifyStringNumber(token).first;
    //se não for, testa se é um string vazio
    else if (token.compare("") == 0)
        return util::TOKEN_TYPE::EMPTY;
    else if (token.compare(" ") == 0)
        return util::TOKEN_TYPE::EMPTY;
    else if (token.compare("\t") == 0)
        return util::TOKEN_TYPE::EMPTY;
    else if (token.compare("\n") == 0)
        return util::TOKEN_TYPE::EMPTY;
    //testa se não é algum caracter especial
    else if (token.compare(":") == 0)
        return util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR;
    else if (token.compare(";") == 0)
        return util::TOKEN_TYPE::COMMENT_MARK;
    else if (token.compare(",") == 0)
        return util::TOKEN_TYPE::ARG_SEPARATOR;
    else if (token.compare("+") == 0)
        return util::TOKEN_TYPE::ADD_SYMBOL;
    //se não for nada disso, testa se é válido
    else if (!isTokenValid(token)){
        return util::TOKEN_TYPE::INVALID;
    }
    //se for válido, testa se é diretiva
    else if (directiveMap->find(token) != directiveMap->end())
        return util::TOKEN_TYPE::DIRECTIVE;
    //ou se é instrução
    else if (instructionMap->find(token) != instructionMap->end())
        return util::TOKEN_TYPE::INSTRUCTION;
    //se não é nada disso, pode ser variável de macro ou variável normal
    else if (token[0] != '&')
        return util::TOKEN_TYPE::SYMBOL;
    else if (token[0] == '&')
        return util::TOKEN_TYPE::MACRO_SYMBOL;
    //se não for nada disso, é algo inválido(espero nunca chegar aqui)
    else{
        return util::TOKEN_TYPE::INVALID;
    }
}
void TokenParser::setMaps(InstructionMap *m, DirectiveMap *mm){
    instructionMap = m;
    directiveMap = mm;
}
