#include "passagemZero.hpp"


void PassagemZero::construct(){
    lines2Skip = 0;
}

void PassagemZero::dealWith_EQU(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    //testa se estamos antes de uma sessão de texto
    int foundSymbolDef = 0;
    for(int i = 0; i < lineTokens.size(); i++){
        if(std::get<1>(lineTokens[i]) == util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR)
            foundSymbolDef++;
    }
    if(foundSymbolDef > 1){
        throw SyntaticError("Dois rótulos ou mais na mesma linha");
    }

    if (textMode == true || dataMode == true)
        throw SemanticError("Diretiva EQU deve ser definida previamente a sessão de texto! Não vou definir a diretiva!");

    auto lineT_only = getTokensInLine(lineTokens);
    if(!isDirectiveLineValid(lineT_only, "equ"))
        throw SyntaticError("Diretiva EQU com formato inválido! Flag não definida!\n" + getDirectiveHelpMsg("equ") +  getCurrentLineInstructionHelp(lineT_only));

    //pega nome da flag em low case
    std::string flag = std::get<0>(lineTokens[0]);
    boost::to_lower(flag);

    //pega valor numerico a setar flag
    int val = util::classifyStringNumber(std::get<0>(lineTokens[3])).second;
    if(flagMap.find(flag) != flagMap.end()) // verifica se label ja nao foi definido
        throw SemanticError("Redefinição de Simbolo '" + flag + "'");
    //coloca no mapa
    flagMap[flag] = val;

}

void PassagemZero::dealWith_IF(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    //testa se estamos não estamos em uma sessão de dados
    if (dataMode == true)
        throw SemanticError("Diretiva IF não pode estar dentro de uma sessão de dados! Vou ignorar o IF!");

    auto lineT_only = getTokensInLine(lineTokens);
    if(!isDirectiveLineValid(lineT_only, "if"))
        throw SyntaticError("Diretiva IF com formato inválido!\n" + getDirectiveHelpMsg("if") +  getCurrentLineInstructionHelp(lineT_only));

    //pega nome da flag(do segundo token) em low case
    std::string flag = std::get<0>(lineTokens[1]);
    boost::to_lower(flag);

    //testa se flag já foi definida
    bool flagExists = flagMap.find(flag) != flagMap.end();
    if(!flagExists){
        throw SemanticError("Diretiva IF requer que a flag já tenha sido definida! Flag lida: " + flag);
    }

    //pega valor da flag
    int val = flagMap[flag];

    //se valor for nulo, simplesmente lemos a linha e a discartamos
    if (val ==  0){
        report("<zero>[skipping line] Flag is false");
        readLine();
    }
}

void PassagemZero::doSomething( std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    if(lineTokens.size() == 0)
        return;

    bool dealt = false;
    for (auto token : lineTokens){
        std::string tokenS= std::get<0>(token);
        util::TOKEN_TYPE tokenT = std::get<1>(token);
        boost::algorithm::to_lower(tokenS);
        if(tokenT == util::TOKEN_TYPE::DIRECTIVE && tokenS.compare("if") == 0){
            dealWith_IF(lineTokens);
            dealt = true;
            break;
        }
        if(tokenT == util::TOKEN_TYPE::DIRECTIVE && tokenS.compare("equ") == 0){
            dealWith_EQU(lineTokens);
            dealt = true;
            break;
        }
    }
    if(dealt == false){
        int symbolDef = 0;
        for(auto tokenDef : lineTokens){
            if(std::get<1>(tokenDef) == util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR)
                symbolDef++;
        }
        // auto tokenAnterior = lineTokens[0]; // usado para tratar redefinicao de simbolo caso precise
        for(auto token : lineTokens){
            bool flagExists = flagMap.find(std::get<0>(token)) != flagMap.end();
            if(flagExists){// se nao quiser que substitua em todo lugar que aparecer, mudar essa parte do código
                // if(symbolDef == 1 && token == tokenAnterior) // vaso precise tratar redefinicao de simbolo no pre processamento
                    // throw SemanticError("Redefinição de símbolo"); 
                if(symbolDef > 1 && dataMode == false && textMode == false) // label duplicada na passagem de EQU
                    throw SyntaticError("Dois ou mais rótulos na mesma linha");
                else{
                    writeToOutput(flagMap.find(std::get<0>(token))->second);
                    symbolDef = false;
                }
            }
            else
                writeToOutput(std::get<0>(token));
            writeSpace();
            // tokenAnterior = token; // usado para tratar redefinicao de simbolo caso precise
        }
        writeEndOfLine();
    }
}

void PassagemZero::dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    PassagemAbstrata::dealWith_Section(lineTokens);
    copyLineToOutput(lineTokens);

}
