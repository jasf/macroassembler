#include "passagem.hpp"

int getNumberOfLinesInFile(string fileName){
    //test if file can be oppened
    ifstream in_aux(fileName);
    if (!in_aux.is_open()){
        throw runtime_error("Cannot open file " + fileName); 
    }
    //count lines in file:
    int i = 0;
    string line;
    while(std::getline(in_aux, line))
        i++;
    in_aux.close();

    return i;
}

void PassagemAbstrata::initiateDefaultIndexMap(string fileName){

    int i = getNumberOfLinesInFile(fileName);

    //initiate deafult map with 0,1,2,....,LineCount
    map<int, int> initialMap;
    for ( int d = 0; d <= i; d++)
        initialMap.insert(std::make_pair(d, d));

    setInputLineMap(initialMap);
}

void PassagemAbstrata::initiateIndexMap(string fileName, map<int, int> mapa){
    int i = getNumberOfLinesInFile(fileName);

    //initiate deafult map with 0,1,2,....,LineCount
    map<int, int> initialMap;
    for ( int d = 0; d <= i; d++){
        if(mapa.find(d) == mapa.end())
            throw runtime_error("Problema com idice de linha! Linha " + std::to_string(d) + " nao encontrada" );
        initialMap.insert(std::make_pair(d, mapa[d]));
    }

    setInputLineMap(initialMap);
}

void PassagemAbstrata::initiateIndexMap(string fileName, vector<int> vec){
    int i = getNumberOfLinesInFile(fileName);

    //initiate deafult map with 0,1,2,....,LineCount
    map<int, int> initialMap;
    for ( int d = 0; d <= i; d++){
        if(d > vec.size())
            throw runtime_error("Problema com indice de linha! Vetor nao contem indice suficientes!");
        initialMap.insert(std::make_pair(d, vec[d-1]));
    }

    setInputLineMap(initialMap);
}

void PassagemAbstrata::incrementPosition(int d){
    inputPositionCounter +=d;
    //report("[pos incr]" + std::to_string(d));
   }

void PassagemAbstrata::incrementLineCounter(int d){
    inputLineCounter+=d;
    //report("[line incr]" + std::to_string(d));
   }

void PassagemAbstrata::construc(std::ifstream *inputFile, std::ofstream *outputFile){
    inputF = inputFile;
    outputF = outputFile;

    inputLineCounter = 0;
    inputPositionCounter = 0;

    outputPositionCounter = 0;
    outputLineCounter = 0;

    logActive = false;
    textMode = false;
    dataMode = false;

    verbosity = 0;
    logTh = 0;

    setLineBreaker(
            [&](std::string line)->std::vector<std::tuple<std::string, util::TOKEN_TYPE> > {
            //primeiro preprocessa a string para acrescentarespaços se necessário
            std::string o = "";

            //define caracteres especiais
            std::set<char> specialS;
            specialS.insert(':');
            specialS.insert(';');
            specialS.insert(',');
            specialS.insert('+');

            //insere espaco em branco antes e depois de caracteres especiais
            for(auto ch:line){
                if (specialS.find(ch) != specialS.end()){
                    o += ' ';
                    o += ch;
                    o += ' ';
                    }
                else
                    o += ch;
            }

            //quebra string em tokens
            std::vector<std::string> strs;
            boost::split(strs, o, boost::is_any_of("\t  "));

            //classifica tokens(converte-0 para minuscula antes)
            std::vector<std::tuple<std::string, util::TOKEN_TYPE> > out  ;
            for(auto token : strs){
                boost::to_lower(token);
                auto ttype = parser->calssifyToken(token);
                //para se econtrar um indicador de comentário
                if (ttype == util::TOKEN_TYPE::COMMENT_MARK)
                    break;
                //não adiciona o token se por acaso ele for um espaço em branco
                if (ttype == util::TOKEN_TYPE::EMPTY)
                    continue;
                out.push_back(std::make_pair(token, ttype));
            }

            return out;
            });
}


PassagemAbstrata::PassagemAbstrata(std::ifstream *inputFile, std::ofstream *outputFile):inputF(inputFile), outputF(outputFile){
    construc(inputFile, outputFile);
}

//para faciliar escrita de modulos teste
PassagemAbstrata::PassagemAbstrata(std::ifstream *inputFile, std::ofstream *outputFile, SymbolTable* st, TokenParser *ps, InstructionMap *im, 
        DirectiveMap *dm){
    construc(inputFile, outputFile);
    setSymbolTable(st);
    setParser(ps);
    setMaps(im, dm);

}

void PassagemAbstrata::execute(){
    try{
        //lê uma linha, se houver definição de label, requer que a label e a definição estejam na mesma linha
        //Isto é, se não estiver na mesma linha, fique puxando prócimas linhas até achar
        // loop will be break by the throw of an exception
        while (true) {
            //read line and get its tokens
            std::string str = readLine();
            std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens = lineBreaker(str);
            //lida com caso em que a label vem na linha anterior a daquela que define
            //por exemplo:
            //labelProblema:
            //  add A 
            if (lineTokens.size() == 2 && std::get<1>(lineTokens[1]) == util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR){
                std::string newline;
                //loop can also be break by an exception
                while(lineTokens.size() == 2){
                    report("<abst>[get nl] looking for symbol definition in next line");
                    newline = readLine();
                    auto newLineTokens = lineBreaker(newline);
                    lineTokens.insert(lineTokens.end(), newLineTokens.begin(), newLineTokens.end());
                }

            }
            try{
                if(util::hasSectionInLine(lineTokens)){
                    dealWith_Section(lineTokens);
                    continue;
                }
                //procure erro lexico
                auto lineT_only = getTokensInLine(lineTokens);
                for(auto i = 0; i < lineT_only.size(); i++){
                    auto t = lineT_only[i];
                    if(t == util::TOKEN_TYPE::INVALID){
                        //so loga se 1 <= logTh
                        log("Erro léxico na linha " + std::to_string(getInputMapedLineCounter()), 1);
                        log("\ttoken problemático: " + getTokenName(lineTokens[i]), 1);
                    }
                }

                doSomething(lineTokens);
            }
            catch(LexicalError E){
                log("Erro léxico na linha " + std::to_string(getInputMapedLineCounter()) + "\n\t" + E.what());  
            }
            catch(SyntaticError E){
                log("Erro sintático na linha " + std::to_string(getInputMapedLineCounter()) + "\n\t" + E.what());  
            }
            catch(SemanticError E){
                log("Erro semântico na linha " + std::to_string(getInputMapedLineCounter()) + "\n\t" + E.what());  
            }
            catch(MultipleErrorsInLine E){
                log(E.myWhat(getInputMapedLineCounter()));
            }
            catch(UnexpectedEOF E){
                log("Fim de arquivo!\n\t" + std::string(E.what()));
                throw E;
            }
            catch(AssemblerError E){
                log("Erro não identificado encontrado! linha " + std::to_string(E.line) + "\n\t" + E.what());  
            }
            catch(std::runtime_error E){
                report(E.what());
                throw E;
            }
        }//end while
    }//end external try
    catch(UnexpectedEOF E){
        //simple ends process
    }
    catch(std::runtime_error E){
        throw E;
    }
    if(existText == false)
        log("Erro semântico: SECTION TEXT faltante",1);
    if(existData == false)
        log("Erro semântico: SECTION DATA faltante",1);
    if(existStop == false)
        log("Erro semântico: programa sem STOP",2);

}

std::string PassagemAbstrata::readLineMacro(int lines2Skip){
    std::string line;
    if (std::getline(*inputF, line)){
        incrementLineCounter(1); 
        return line;
    }
    else{
        incrementLineCounter(lines2Skip*(-1));
        throw SemanticError("Definicao de macro sem ENDMACRO!");
    }
}

std::string PassagemAbstrata::readLine(){
    std::string line;
    if (std::getline(*inputF, line)){
        incrementLineCounter(1); 
        return line;
    }
    else
        throw UnexpectedEOF("Arquivo chegou ao fim!");
}

void PassagemAbstrata::writeToOutput(std::string s){
    *outputF << s;
    outputPositionCounter += 1;
}

void PassagemAbstrata::writeToOutput(int d){
    writeToOutput(std::to_string(d));
}

void PassagemAbstrata::writeToOutputSpace(std::string s){
    writeToOutput(s);
    writeSpace();
}

void PassagemAbstrata::writeToOutputSpace(int d){
    writeToOutput(d);
    writeSpace();
}

void PassagemAbstrata::writeEndOfLine(){
    *outputF << std::endl;
    outputLineVec.push_back(inputLineMap[getCurrentILineCounter()]);
    outputLineCounter+= 1;
}

std::vector<util::TOKEN_TYPE> PassagemAbstrata::getTokensInLine(vector<tuple<string, util::TOKEN_TYPE>> lineTokens){
    vector<util::TOKEN_TYPE> v;
    for(auto t : lineTokens)
        v.push_back(get<1>(t));
    return v;
}

std::set<util::TOKEN_TYPE> PassagemAbstrata::getTokenSetInLine(std::vector<std::tuple<std::string, util::TOKEN_TYPE>> lineTokens){
    auto v = getTokensInLine(lineTokens);
    set<util::TOKEN_TYPE> s(v.begin(), v.end());
    return s;
}

void PassagemAbstrata::setTextModeOn(){
    textMode = true;
    dataMode = false;
    existText = true;
}

void PassagemAbstrata::setDataModeOn(){
    dataMode = true;
    textMode = false;
    existData = true;
}

void PassagemAbstrata::dealWith_Section(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens){
    report("[section]");
    //testa se há algo depois de section
    if(lineTokens.size() != 2)
        throw SyntaticError("Uso inválido de diretiva section! Esperava-se um token depois(na mesma linha) e nada mais");

    //pega segundo token
    auto secondTokenS = std::get<0>(lineTokens[1]);

    //testa se texto é 'text' ou 'data'
    if ( secondTokenS.compare("text") != 0 && secondTokenS.compare("data") !=0)
        throw SyntaticError("Uso inválido de diretiva section! Esperava-se ser seguida de 'text' ou 'data'");
    //testa se tem algo depois
    if ( lineTokens.size() > 2)
        throw SyntaticError("Uso inválido de diretiva section! Esperava-se apenas mais um token depois de 'section'"); 

    //ativa modo correspondente
    if (secondTokenS.compare("text") == 0){
        if(textMode == true){
            // setTextModeOn();
            log("Erro semântico na linha " + std::to_string(getInputMapedLineCounter()) + "\n\t Definição dupla de SECTION TEXT",1);
            //throw SemanticError("Definicao dupla de section text!"); 
        }
        else
            setTextModeOn();
    }
    else if(secondTokenS.compare("data") == 0){
        if(dataMode == true){
            log("Erro semântico na linha " + std::to_string(getInputMapedLineCounter()) + "\n\t Definição dupla de SECTION DATA",1);
            //throw SemanticError("Definicao dupla de section text!"); 
        }
        else if(textMode == false){
            setDataModeOn();
            log("Erro semântico na linha " + std::to_string(getInputMapedLineCounter()) + "\n\tSECTION DATA deve vir depois da SECTION TEXT !",1);
            //throw SemanticError("Section data deve vir depois da section text!");    
        }
        else
            setDataModeOn();
    }
}
void PassagemAbstrata::copyLineToOutput(std::vector<tuple<string, util::TOKEN_TYPE>> v){
    unsigned int i = 0;
    for( ; i < v.size() - 1; i++){
        auto t = getTokenName(v[i]);
        writeToOutputSpace(t);
    }
    auto t = getTokenName(v[i]);
    writeToOutput(t);
    writeEndOfLine();
}


string PassagemAbstrata::isInstructionSymbolValid(std::vector<std::tuple<std::string, util::TOKEN_TYPE> > lineTokens, string instrucName, SymbolTableData data){
    
    if(instrucName == "jmpp" || instrucName == "jmpz" || instrucName == "jmp" || instrucName == "jmpn"){
        if(lineTokens.size() > 2){
            return "jumpsintatico";
        }
        else if(data.fromSectionData == true){
            return "jumpsemantico";
        }
    }
    else if(instrucName == "div"){
        if(data.isConst == 0){// -1 = isnt const, if 0 or 1 -> is const
            return "div";
        }
        else if(data.fromSectionData == false){//from section text
            return "instruc";
        }
    }
    else if(instrucName == "store"){
        if(data.isConst != -1 || data.fromSectionData == false){// -1 = isnt const, if 0 or 1 -> is const
            return "store";
        }
        else if(data.fromSectionData == false){//from section text
            return "instruc";
        }
    }
    else if(instrucName == "input"){
        if(data.isConst != -1 || data.fromSectionData == false){// -1 = isnt const, if 0 or 1 -> is const
            return "input";
        }
        else if(data.fromSectionData == false){//from section text
            return "instruc";
        }
    }
    else if(instrucName == "copy"){ // COPY A + 1 , B + 2 (5 quando tam = 8 ou = 5) ou COPY A + 1, B / copy A , B + 1 ou COPY A, B 
        SymbolTableData data_copy = symbolTable->getSymbolDataNoError(std::get<0>(lineTokens[3]));        
        SymbolTableData data_copy1 = symbolTable->getSymbolDataNoError(std::get<0>(lineTokens[1])); 
        if(std::get<1>(lineTokens[3]) == util::TOKEN_TYPE::NUM_DEC){
            data_copy = symbolTable->getSymbolDataNoError(std::get<0>(lineTokens[5]));
        }    
        if(data_copy.isConst != -1){// -1 = isnt const, if 0 or 1 -> is const
            if(data_copy1.fromSectionData == false){
                return "copy2";
            }
            else
                return "copy1";
        }
        else if(data_copy.fromSectionData == false || data_copy1.fromSectionData == false){//from section text
            return "instruc";
        }
    }
    else{
        if(data.fromSectionData == false){//from section text
            return "instruc";
        }
    }
    return "true";
}

bool PassagemAbstrata::isInstructionLineValid(vector<util::TOKEN_TYPE> line, string instructName){
    if(instructionMap->find(instructName) == instructionMap->end())
            return false;

    auto possibilities = (*instructionMap)[instructName].expectedOrder;
    if(possibilities.size() == 0)
        return false;

    if(instructName != "jmpp" && instructName != "jmpz" && instructName != "jmp" && instructName != "jmpn"){
        // INSTRUCTION SYMBOL + NUM_DEC
        if(line.size() == 4 && line[1] == util::TOKEN_TYPE::SYMBOL &&
            line[2] == util::TOKEN_TYPE::ADD_SYMBOL &&
            line[3] == util::TOKEN_TYPE::NUM_DEC){
            return true;
        }
        // COPY SYMBOL + NUM_DEC ARG_SEPARATOR SYMBOL + NUM_DEC
        // OR
        // COPY SYMBOL + NUM_DEC ARG_SEPARATOR SYMBOL
        // OR
        // COPY SYMBOL ARG_SEPARATOR SYMBOL + NUM_DEC
        if((line.size() == 8 && line[1] == util::TOKEN_TYPE::SYMBOL && 
            line[2] == util::TOKEN_TYPE::ADD_SYMBOL && line[3] == util::TOKEN_TYPE::NUM_DEC &&
            line[4] == util::TOKEN_TYPE::ARG_SEPARATOR && line[5] == util::TOKEN_TYPE::SYMBOL && 
            line[6] == util::TOKEN_TYPE::ADD_SYMBOL && line[7] == util::TOKEN_TYPE::NUM_DEC) ||

            (line.size() == 6 && line[1] == util::TOKEN_TYPE::SYMBOL && 
            line[2] == util::TOKEN_TYPE::ADD_SYMBOL && line[3] == util::TOKEN_TYPE::NUM_DEC &&
            line[4] == util::TOKEN_TYPE::ARG_SEPARATOR && line[5] == util::TOKEN_TYPE::SYMBOL) ||

            (line.size() == 6 && line[1] == util::TOKEN_TYPE::SYMBOL && 
            line[2] == util::TOKEN_TYPE::ARG_SEPARATOR && line[3] == util::TOKEN_TYPE::SYMBOL &&
            line[4] == util::TOKEN_TYPE::ADD_SYMBOL && line[5] == util::TOKEN_TYPE::NUM_DEC)){
            return true;
        }
    }
    
    for( auto possibility : possibilities){
        bool valid = true;
        if( possibility.size() != line.size())
            valid = false;
        else{
            for(int i = 0; i < possibility.size(); i++){
                auto expectedIns = possibility[i];
                auto gotIns = line[i];
                if(gotIns != expectedIns){
                    valid = false;
                    break;
                }
            }
            //se chagamos ate aqui com valid = true; eh pq a linha bate com a especificacao
            if(valid)
                return true;
        }
    
    }
    return false;
}

bool PassagemAbstrata::isDirectiveLineValid(vector<util::TOKEN_TYPE> line, string instructName){
    if(directiveMap->find(instructName) == directiveMap->end())
            return false;

    auto possibilities = (*directiveMap)[instructName].expectedOrder;
    if(possibilities.size() == 0)
        return false;

    //a marioria das diretivas funcionam como instruções: checa tamanho e tipo posição a posição
    if(instructName.compare("macro") != 0){
        for( auto possibility : possibilities){
                bool valid = true;
                if( possibility.size() != line.size())
                    valid = false;
                else{
                    for(int i = 0; i < possibility.size(); i++){
                        auto expectedIns = possibility[i];
                        auto gotIns = line[i];
                        if(gotIns != expectedIns){
                            valid = false;
                            break;
                        }
                    }
                    //se chagamos ate aqui com valid = true; eh pq a linha bate com a especificacao
                    if(valid)
                        return true;
                }
            }
    }
    //macro no entanto, tem o caso especial da sequência finita mas arbritária de parâmetros
    else{
        for(auto possibility : possibilities){
            bool valid = true;
            for(int i = 0; i < possibility.size(); i++){
                auto expectTT = possibility[i];
                auto gottenTT = line[i];
                //se for sequencia de var, testa se resto da linha é seq de var
                //se for, já retorna da função aqui
                if(expectTT == util::TOKEN_TYPE::MACRO_VAR_SEQ){
                    vector<util::TOKEN_TYPE> restoDaLinha(line.begin()+i, line.end());
                    if(isMacroVarSeq(restoDaLinha)){
                        return true;
                    }
                }
                else if(gottenTT != expectTT){
                    valid = false;
                    break;
                }
            }//end para cada token na possibilidade
            //somente a macro que não possui argumentos chegará nesse ponto
            if(valid){
                if(possibility.size() == line.size())
                    return true;
            }//end se chegamos ao final com flag valid == TRUE
        }//end para cada possibilidade
    }//end else do if 'macro'

    return false;

}


string PassagemAbstrata::getInstructionHelpMsg(string instruction){
    if(instructionMap->find(instruction) == instructionMap->end()){
        throw SyntaticError("[instruct help msg]Instrução " + instruction + "não definida!");
    }
    auto spec = (*instructionMap)[instruction].expectedOrder;
    string msg = "\tSintaxe possível:\n\t\t";
    for(auto p : spec){
        for(auto tt : p)
            msg = msg + util::toString(tt) + " ";
        msg = msg + "\n\t\t";
    }

    return msg;
}

string PassagemAbstrata::getDirectiveHelpMsg(string instruction){
    if(directiveMap->find(instruction) == directiveMap->end()){
        throw SyntaticError("[directive help msg]Instrução " + instruction + "não definida!");
    }
    auto spec = (*directiveMap)[instruction].expectedOrder;
    string msg = "Sintaxe possível:\n\t\t";
    for(auto p : spec){
        for(auto tt : p)
            msg = msg + util::toString(tt) + " ";
        msg = msg + "\n\t\t";
    }

    return msg;
}

string PassagemAbstrata::getCurrentLineInstructionHelp(vector<util::TOKEN_TYPE> ltt){
    string msg = "\n\ttokens lidos: \n\t\t";
    for(auto t : ltt)
        msg = msg + toString(t) + " ";
    msg = msg;
    return msg;
}

string PassagemAbstrata::getCurrentLineInstructionHelp(vector<tuple<string, util::TOKEN_TYPE>> ltt){
    auto ttl_v = getTokensInLine(ltt);
    return getCurrentLineInstructionHelp(ttl_v);
}

bool PassagemAbstrata::isMacroVarSeq(vector<util::TOKEN_TYPE> seq){
    //uma sequencia vazia não é uma sequencia de variaveis de macro
    if(seq.size() == 0)
        return false;

    //até o penúltimo elemento, devemos ter uma sequencia de macro_var, arg_separator
    
    for(unsigned int i = 0; i < seq.size()-1; i+= 2){
        auto t = seq[i];
        auto next = seq[i+1];
        if(t != util::TOKEN_TYPE::MACRO_SYMBOL || next != util::TOKEN_TYPE::ARG_SEPARATOR)
            return false;
    }

    //o ultimo token deve ser uma macro_var
    //(o caso de apenas uma var está incluido aqui)
    if(seq[seq.size()-1] != util::TOKEN_TYPE::MACRO_SYMBOL)
        return false;

    return true;
}
