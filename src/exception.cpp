#include "exception.hpp"


string MultipleErrorsInLine::myWhat(int lineNumber){
    string msg = "";
    for(auto err : errors){
        if(err.getType() == ErrType::LEXICAL)
            msg +="Erro léxico na linha " + std::to_string(lineNumber) + "\n\t" + err.what() + "\n\n"; 
        else if(err.getType() == ErrType::SYNTATIC)
            msg +="Erro sintático na linha " + std::to_string(lineNumber) + "\n\t" + err.what() + "\n\n"; 
        else if(err.getType() == ErrType::SEMANTIC)
            msg +="Erro semântico na linha " + std::to_string(lineNumber) + "\n\t" + err.what() + "\n\n"; 
    }
    return msg;

}
