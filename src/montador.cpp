#include <iostream>
#include <string>
#include <iostream>
#include <tuple>
#include <fstream>
#include <stdexcept>
#include <set>
#include <vector>
#include <map>
#include <stdio.h>

#include "util.hpp"
#include "tabelas.hpp"
#include "tokenParser.hpp"
#include "passagem.hpp"
#include "passagemZero.hpp"
#include "passagemMacro.hpp"
#include "passagemMontagem.hpp"

using namespace std;

void help(){
    cout << "usage: ./montador -op BaseNameInput BaseNameOutput [-v verbosity] " << endl;
    cout << "op assume um dos valores: 'p', 'm' ou 'o' " << endl;
    cout << "-v verbosity: parametros opcionais; verbosity deve ser numeral inteiro. ex.: -v 0; -v 10" << endl;
    cout << "Use 0 para ver somente o excencial" << endl;
}

int main(int argc, char **argv){
    if(argc < 4){
        cout << "[ERRO] Quantidade de argumentos incorreta" << endl;
        help();
        exit(1);
    }
    try{
        //cria strings com argumentos de entrada
        auto argumentos = util::parseInput(argv, argc);
        auto op = util::getOpType(argumentos["modo"]);
        string baseNameIn(argumentos["entrada"]);
        string baseNameOut(argumentos["saida"]);
        int verbosity = std::stoi(argumentos["verbosity"]);

        set<string> op2Realize;

        switch(op){
            case util::OP_TYPE::INVALID :
                help();
                throw new runtime_error("modo inválido!");
                break;
            case util::OP_TYPE::MONT :
                op2Realize.insert("mont");
            case util::OP_TYPE::MCR :
                op2Realize.insert("mcr");
            case util::OP_TYPE::PRE :
                op2Realize.insert("pre");
            default:
                break;
        }

        //inicializa variaveis dividias por todos os metodos
        auto dTable = util::getDiretivasMap();
        auto iTable = util::getInstructionMap();
        SymbolTable sTable;
        TokenParser parser;
        parser.setMaps(&iTable, &dTable);

        ifstream in;
        ofstream out;

        vector<pair<vector<int>, string>> maps2Print;

        if (op2Realize.find("pre") != op2Realize.end()){

            cout << "\nExecutando passagem zero para resolver IF e EQU ... \n" << endl;
            in.open(baseNameIn + ".asm"); out.open(baseNameOut + ".pre");

            PassagemZero p0(&in, &out, &sTable, &parser, &iTable, &dTable);
            // inicializar o mapa de numero de linhas do arquivo inicial (input)  
            p0.initiateDefaultIndexMap(baseNameIn + ".asm");
            p0.setVerbosity(verbosity);
            //vai logar erro lexico
            p0.setLogControl(1);
            p0.toogleLog();
            p0.execute();

            //get maps to print 
            maps2Print.push_back(make_pair(p0.getOutputLineVector(), "zero/in"));

        //esse if está dentro do if 'pre'
        if (op2Realize.find("mcr") != op2Realize.end()){
            cout << "\nExecutando primeira passagem de macro ... \n" << endl;
            in.close(); out.close();
            in.open(baseNameOut + ".pre"); out.open(baseNameOut + "_aux.mcr");

            //inicializa e roda
            PassagemMacro_Learn pm0(&in, &out, &p0);
            pm0.initiateDefaultIndexMap(baseNameOut + ".pre");

            pm0.setVerbosity(verbosity);
            pm0.toogleLog();
            pm0.execute();

            //imprime resultados
            pm0.printTables();


            cout << "\nExecutando segunda passagem de macro ... \n" << endl;
            in.close(); out.close();
            in.open(baseNameOut + "_aux.mcr"); out.open(baseNameOut + ".mcr");
            //inicializa e roda
            PassagemMacro_Solve pm1(&in, &out, &pm0);
            if(!(in.peek() == std::ifstream::traits_type::eof())){

                //pm1.initiateDefaultIndexMap(baseNameOut + "_aux.mcr");
                pm1.initiateIndexMap(baseNameOut + "_aux.mcr", pm0.getOutputLineVector());
                pm1.setVerbosity(verbosity);
                //vai logar erro lexico -> acho que nao precisa logar 
                // pm1.setLogControl(1);
                pm1.toogleLog();
                pm1.execute();
            
                //get maps to print 
                maps2Print.push_back(make_pair(pm0.getOutputLineVector(), "mcr0/zero"));
                maps2Print.push_back(make_pair(pm1.getOutputLineVector(), "mcr1/mcr0"));
            }

        //esse if está dentro do if 'mcr'
            
            in.close(); out.close();
            in.open(baseNameOut + ".mcr"); out.open(baseNameOut + "_aux.o");

        if (op2Realize.find("mont") != op2Realize.end()){
            cout << "\nExecutando primeira passagem de montagem ... \n" << endl;
            //inicializa e executa
            PassagemMontagem_Learn pml(&in, &out, &pm1);
            pml.initiateDefaultIndexMap(baseNameOut + ".mcr");
            pml.setVerbosity(verbosity);
            pml.toogleLog();
            pml.execute();

            //imprime resultados
            pml.printSymbolTable();

            cout << "\nExecutando segunda passagem de montagem ... \n" << endl;
            in.close(); out.close();
            in.open(baseNameOut + "_aux.o"); out.open(baseNameOut + ".o");
            if(!(in.peek() == std::ifstream::traits_type::eof())){
                PassagemMontagem_Solve pms(&in, &out, &pml);
                //pms.setInputLineMap(pml.getOutputLineVector());
                pms.initiateIndexMap(baseNameOut + "_aux.o", pml.getOutputLineVector());
                pms.setVerbosity(verbosity);
                //vai logar erro lexico
                pms.setLogControl(2);
                pms.toogleLog();
                pms.execute();

                in.close(); out.close();

                //get maps to print 
                //get maps to print 
                maps2Print.push_back(make_pair(pml.getOutputLineVector(), "mnt0/mcr1"));
                maps2Print.push_back(make_pair(pms.getOutputLineVector(), "mnt1/mnt0"));
            }

        }/*end if montagem*/ } /*end if macro*/ } /*end if pre*/
        //imprime mapas que foram gerados
        if(verbosity > 5){
            for (auto mapa : maps2Print)
                util::printMap(mapa.first, mapa.second);
        }
        util::removeTempFiles(baseNameOut + "_aux.mcr");
        util::removeTempFiles(baseNameOut + "_aux.o");
    } catch(std::runtime_error *E){
        cout << E->what() << endl;
    }

    return 0;
}
