#include "util.hpp"


void util::removeTempFiles(std::string temp_file){
    remove(temp_file.c_str());
}

std::map<std::string, util::InstructionSpec> util::getInstructionMap(){
    std::map<std::string, util::InstructionSpec> out;
    out["add"]    = util::InstructionSpec(1,1,2);
    out["sub"]    = util::InstructionSpec(1,2,2);
    out["mult"]   = util::InstructionSpec(1,3,2);
    out["div"]    = util::InstructionSpec(1,4,2);
    out["jmp"]    = util::InstructionSpec(1,5,2);
    out["jmpn"]   = util::InstructionSpec(1,6,2);
    out["jmpp"]   = util::InstructionSpec(1,7,2);
    out["jmpz"]   = util::InstructionSpec(1,8,2);
    out["copy"]   = util::InstructionSpec(2,9,3);
    out["load"]   = util::InstructionSpec(1,10,2);
    out["store"]  = util::InstructionSpec(1,11,2);
    out["input"]  = util::InstructionSpec(1,12,2);
    out["output"] = util::InstructionSpec(1,13,2);
    out["stop"]   = util::InstructionSpec(0,14,1);

    vector<TOKEN_TYPE> addSpec;
    addSpec.push_back(util::TOKEN_TYPE::INSTRUCTION);
    addSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    out["add"].addPossiblePath(addSpec);
    out["sub"].addPossiblePath(addSpec);
    out["mult"].addPossiblePath(addSpec);
    out["div"].addPossiblePath(addSpec);
    out["jmp"].addPossiblePath(addSpec);
    out["jmpn"].addPossiblePath(addSpec);
    out["jmpp"].addPossiblePath(addSpec);
    out["jmpz"].addPossiblePath(addSpec);
    out["load"].addPossiblePath(addSpec);
    out["store"].addPossiblePath(addSpec);
    out["input"].addPossiblePath(addSpec);
    out["output"].addPossiblePath(addSpec);


    vector<TOKEN_TYPE> copySpec;
    copySpec.push_back(util::TOKEN_TYPE::INSTRUCTION);
    copySpec.push_back(util::TOKEN_TYPE::SYMBOL);
    copySpec.push_back(util::TOKEN_TYPE::ARG_SEPARATOR);
    copySpec.push_back(util::TOKEN_TYPE::SYMBOL);
    out["copy"].addPossiblePath(copySpec);


    vector<TOKEN_TYPE> stopSpec;
    stopSpec.push_back(util::TOKEN_TYPE::INSTRUCTION);
    out["stop"].addPossiblePath(stopSpec);

    return out;
}

std::map<std::string, util::InstructionSpec> util::getDiretivasMap(){
    std::map<std::string, util::InstructionSpec> out;
    out["section"]      = util::InstructionSpec(1  ,-1,0);
    out["space"]        = util::InstructionSpec(-1 ,-1,-1);
    out["const"]        = util::InstructionSpec(1  ,-1,1);
    out["equ"]          = util::InstructionSpec(1  ,-1,0);
    out["if"]           = util::InstructionSpec(1  ,-1,0);
    out["macro"]        = util::InstructionSpec(0  ,-1,0);
    out["endmacro"]     = util::InstructionSpec(0  ,-1,0);

    //section nome
    vector<TOKEN_TYPE> sectionSpec;
    sectionSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);
    sectionSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    out["section"].addPossiblePath(sectionSpec);


    //label: space 
    //label: space N 
    vector<TOKEN_TYPE> spaceSpec;
    spaceSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    spaceSpec.push_back(util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR);
    spaceSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);
    out["space"].addPossiblePath(spaceSpec);
    spaceSpec.push_back(util::TOKEN_TYPE::NUM_DEC);
    out["space"].addPossiblePath(spaceSpec);

    //label: const DEC
    vector<TOKEN_TYPE> constSpec;
    constSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    constSpec.push_back(util::TOKEN_TYPE::ARG_SEPARATOR);
    constSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);
    constSpec.push_back(util::TOKEN_TYPE::NUM_DEC);
    out["space"].addPossiblePath(constSpec);

    //label: equ DEC
    //label: equ HEX
    vector<TOKEN_TYPE> equSpec;
    equSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    equSpec.push_back(util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR);
    equSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);
    equSpec.push_back(util::TOKEN_TYPE::NUM_DEC);
    out["equ"].addPossiblePath(equSpec);
    equSpec.pop_back();
    equSpec.push_back(util::TOKEN_TYPE::NUM_HEX);
    out["equ"].addPossiblePath(equSpec);

    //if simbolo
    vector<TOKEN_TYPE> ifSpec;
    ifSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);
    ifSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    out["if"].addPossiblePath(ifSpec);

    //endmacro
    vector<TOKEN_TYPE> endMacroSpec;
    endMacroSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);

    //label: macro 
    //label: macro &A, &B, ... , &Z
    vector<TOKEN_TYPE> macroSpec;
    macroSpec.push_back(util::TOKEN_TYPE::SYMBOL);
    macroSpec.push_back(util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR);
    macroSpec.push_back(util::TOKEN_TYPE::DIRECTIVE);
    out["macro"].addPossiblePath(macroSpec);
    macroSpec.push_back(util::TOKEN_TYPE::MACRO_VAR_SEQ);
    out["macro"].addPossiblePath(macroSpec);


    return out;
}

std::string util::toString(util::TOKEN_TYPE e){
    switch(e){
        case util::TOKEN_TYPE::INVALID :
            return "INVALID";
            break;
        case util::TOKEN_TYPE::NUM_DEC :
            return "NUM_DEC";
            break;
        case util::TOKEN_TYPE::NUM_HEX :
            return "NUM_HEX";
            break;
        case util::TOKEN_TYPE::SYMBOL_DEFINITION_INDICATOR :
            return "SYMBOL_DEFINITION_INDICATOR";
            break;
        case util::TOKEN_TYPE::MACRO :
            return "MACRO";
            break;
        case util::TOKEN_TYPE::SYMBOL :
            return "SYMBOL";
            break;
        case util::TOKEN_TYPE::DIRECTIVE :
            return "DIRECTIVE";
            break;
        case util::TOKEN_TYPE::INSTRUCTION :
            return "INSTRUCTION";
            break;
        case util::TOKEN_TYPE::COMMENT_MARK:
            return "COMMENT_MARK";
            break;
        case util::TOKEN_TYPE::ARG_SEPARATOR:
            return "ARG_SEPARATOR";
            break;
        case util::TOKEN_TYPE::MACRO_VAR_SEQ:
            return "MACRO_VAR_SEQ";
            break;
        case util::TOKEN_TYPE::MACRO_SYMBOL:
            return "MACRO_SYMBOL";
            break;
        case util::TOKEN_TYPE::ADD_SYMBOL:
            return "ADD_SYMBOL";
            break;
        default:
            return "invalid! Type non defined!";
            break;
    }

}

//retorna true caso string contenha numero hexadecimal
//[retirado de http://stackoverflow.com/questions/8899069/how-to-find-if-a-given-string-conforms-to-hex-notation-eg-0x34ff-without-regex
//resposta por Rob Kennedy em Jan 17 '12 at 17:42
//modificado para aceitar formato -0x...
bool is_hex_notation(std::string const& s) {
  return ((s.size() > 2 &&
          s.compare(0, 2, "0x") == 0 && 
          s.find_first_not_of("0123456789abcdefABCDEF", 2) == std::string::npos)
      ||(s.size() > 3 &&
      s.compare(0, 3, "-0x") == 0 &&
       s.find_first_not_of("0123456789abcdefABCDEF", 3) == std::string::npos));
}

std::pair<util::TOKEN_TYPE, int> util::classifyStringNumber(std::string in){
    try{
        int in_d = boost::lexical_cast<int>(in);
        return std::make_pair(util::TOKEN_TYPE::NUM_DEC, in_d);
    }
    catch(boost::bad_lexical_cast){
        if(is_hex_notation(in)){
            int x;   
            std::stringstream ss;
            ss << std::hex << in;
            ss >> x;
            return std::make_pair(util::TOKEN_TYPE::NUM_HEX, x);
        }
        else
            return std::make_pair(util::TOKEN_TYPE::INVALID, -99);

    }

}

void util::printMap(std::vector<int> map, std::string name){
    std::cout << "Mapemento " << name << std::endl;
    std::cout << " saida : entrada" << std::endl;

    for(int k = 0; k < map.size(); k++)
        std::cout << k+1 << " " << map[k] << std::endl;

}

void util::waitKey(){
//codes here
    std::cout << "Press anykey to continue." << std::endl;
    std::cin.ignore();
}

util::OP_TYPE util::getOpType(std::string s){
    if (s.compare("-p") == 0)
        return util::OP_TYPE::PRE;
    else if (s.compare("-m") == 0)
        return util::OP_TYPE::MCR;
    else if (s.compare("-o") == 0)
        return util::OP_TYPE::MONT;
    else 
        return util::OP_TYPE::INVALID;
 
}

std::string util::getTokenName(std::tuple<std::string, util::TOKEN_TYPE> t){
    return std::get<0>(t);
}

util::TOKEN_TYPE util::getTokenType(std::tuple<std::string, util::TOKEN_TYPE> t){
    return std::get<1>(t);
}
bool util::hasSectionInLine(vector<tuple<string, TOKEN_TYPE>> line){
    for(auto t : line){
        auto tokenT = getTokenType(t);
        auto tokenN = getTokenName(t);
        if(tokenT == util::TOKEN_TYPE::DIRECTIVE &&  tokenN.compare("section") == 0){
            return true;
        }
    }
    return false;
}
void util::printLineToScreen(vector<tuple<string, TOKEN_TYPE>> line){
    for( auto t : line)
        cout << getTokenName(t) << " ";
    cout << endl;

}

string util::toString(vector<tuple<string, TOKEN_TYPE>> line){
    string s = "";
    for( auto t : line)
        s += getTokenName(t) + " " ;

    return s;
}


map<string, string> util::parseInput(char** argv, int argc){
    map<string, string> mapa;
    mapa["modo"]    = argv[1];
    mapa["entrada"] = argv[2];
    mapa["saida"]   = argv[3];
    mapa["verbosity"] = "0";
    if((argc - 4) >=  2 && string(argv[4]).compare("-v") == 0)
        mapa["verbosity"] = argv[5];


    return mapa;
    }

map<int, int> util::createLineMap(vector<int> inputLines, vector<int> outputLines){
    map<int, int> out;

    for(auto outL : outputLines){
        out[outL] = inputLines[outL-1];
    }

    return out;
}

// bool is_empty(std::ifstream& cin){
//     return std::cin.peek() == std::ifstream::traits_type::eof();
// }